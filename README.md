# Gnome Arduino IDE

![Screenshot](screenshots/logo.png?raw=true )

Icon taken from https://www.flaticon.com/free-icon/pawprint_22724, thanks to flaticon

A nice IDE for developers of Arduino, integrated with Gnome

## TODO
- [x] Modern UI
- [x] Compile and upload
- [x] Cancel compile/Upload
- [x] Autocomplete code
- [x] Serial Monitor
- [ ] Serial plotter
- [x] Multiple serial monitor
- [x] Multiple boards handler
- [~] Packages manager
- [ ] Transite from current boards handler to bd handler
- [ ] Pin assignement window
- [ ] Base conversor
- [ ] Text editor plugins
- [ ] Translated
- [ ] Git handler
- [ ] Separe repositories by category, for example "Arduino", "Comunity"

## Dependencies
- `gtk3 >= 3.20`
- `gobject-introspection`
- `shutil`
- `python3`
- `pip3`
- `appfiles`
- `pip3 install serial`
- `arduino-builder >= 1.3` (Optional) https://github.com/arduino/arduino-builder
- `pip3 install urllib`

## Building from git
excecute as normal user, NO superuser

```bash
python install.py
```

if you don't have the $HOME var set, then set this

```bash
export HOME=/home/user
```
where user is your username

## On Arch Linux
```bash
# pacman -S arduino-builder
```

## On Debian (Jessie)
```bash
# apt-get install libglib2.0-dev libgtk-3-dev
```
## License
Code is under GNU GPLv3 license.

## Screenshots
![Screenshot](screenshots/screen1.png?raw=true )
![Screenshot](screenshots/screen2.png?raw=true )
![Screenshot](screenshots/screen4.png?raw=true )

## Design
![Screenshot](screenshots/screen3.png?raw=true )
![Screenshot](screenshots/screen5.png?raw=true )

