'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''
import sys
import os
from shutil import copyfile, copytree, rmtree

home = str(os.environ['HOME'])

if home == "/root" or home == "/home/root" or home == "" or home == None:
	print("\033[91mYou may not run this script as superuser\033[0m, for install run:\n\033[92m python install.py \033[0m\n")
	print("Remember to set the HOME variable, export HOME=/home/user, where user is your user name")
	sys.exit(0)

print("copying arduino files into "+str(home)+"/.config and into "+str(home)+"/.ardunome")

if not os.path.isdir(home+"/.config"):
	os.makedirs(home+"/.config")
if not os.path.isdir(home+"/.config/ardunome"):
	os.makedirs(home+"/.config/ardunome")
	
copyfile("data/style.css", home+"/.config/ardunome/style.css")
copyfile("data/commands.txt", home+"/.config/ardunome/commands.txt")

if os.path.isdir(home+"/.ardunome"):
	rmtree(home+"/.ardunome")
print("copying build tools this may take a while, please don't interrupt this operation")
copytree("build-tools", home+"/.ardunome")

string = "sudo python setup.py install"
os.system(string)
