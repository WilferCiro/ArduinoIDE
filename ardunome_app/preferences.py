'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

from gi.repository import Gtk, Gio, GLib

import shelve
import os, errno

try:
	from functions import return_ui_file, App, APP_NAME
except:
	from ardunome_app.functions import return_ui_file, App, APP_NAME

# TODO: change compile paths
# FIXME: change to platforms.txt 

class preferences(Gtk.Window):
	"""
        Init text editor
    """
	def __init__(self):
		"""
            Init source view
        """
		Gtk.Window.__init__(self)
		
		# Configure file
		path = return_ui_file("preferences_menu.ui")
		self.builder = Gtk.Builder()
		self.builder.add_from_file(path)
		self.builder.connect_signals(self)
		
		self.connect("delete_event", self.close_preferences)
		self.set_position(Gtk.WindowPosition.CENTER)
		self.set_default_size(650, 200)
		
		self.__header = self.builder.get_object("PreferencesHeader")
		self.__body = self.builder.get_object("PreferencesBody")
		self.set_titlebar(self.__header)
		self.set_modal(True)
		self.add(self.__body)
		
		# Connects
		buton_save = self.builder.get_object("SavePreferences")
		buton_save.connect("clicked", self.save_preferences)
		# Objects
		self._show_compile_output = self.builder.get_object("ShowCompileOutput")
		self._show_upload_output = self.builder.get_object("ShowUploadOutput")
		self._dark_theme = self.builder.get_object("DarkTheme")
		self._editor_font = self.builder.get_object("EditorFont")
		self._libraries_path = self.builder.get_object("LibrariesPath")
		self._packages_path = self.builder.get_object("PackagesPath")
		self._keep_output_files = self.builder.get_object("KeepOutputFiles")
		self._builder_path_file = self.builder.get_object("BuilderPath")
		self._editor_theme = self.builder.get_object("EditorTheme")
		
		# Settings Object
		self.setting_compile = "compile_verbose"
		self.setting_upload = "upload_verbose"
		self.text_editor = "font_editor"
		self.packages_path = "packages_path"
		self.libraries_paths = "libraries_paths"
		self.keep_output = "keep_output"
		self.path_builder = "builder_path"
		self.examples_path = "examples_path"
		self.dark_theme = "dark_theme"
		self.editor_theme = "editor_theme"
		self.repository_url = "repository_url"
		
		self.dir_settings = GLib.get_user_config_dir() + "/" + APP_NAME + "/"
		self.file_name = "config.txt"
		if not os.path.isfile(self.dir_settings + self.file_name):
			self.default_config()
		
		self._libraries_path.set_filename(self.get_value(self.libraries_paths))
		self._packages_path.set_filename(self.get_value(self.packages_path))
		self._editor_font.set_font(self.get_value(self.text_editor))
		
		self._keep_output_files.set_active(self.get_value(self.keep_output))
		self._show_compile_output.set_active(self.get_value(self.setting_compile))
		self._show_upload_output.set_active(self.get_value(self.setting_upload))
		
		self._builder_path_file.set_filename(self.get_value(self.path_builder))
		
		self._dark_theme.set_active(self.get_value(self.dark_theme))
		
		self.__reset_font_button = self.builder.get_object("ResetFont")
		self.__reset_font_button.connect("clicked", self.reset_font)
		
	
	def reset_font(self, *args):
		self._editor_font.set_font("Monospace 11")
		
	def close_preferences(self, *args):
		return self.hide_on_delete()
	
	def start(self):
		self.show_all()
		
	def save_preferences(self, *args):
		ShowCompileOutput = self._show_compile_output.get_active()
		ShowUploadOutput = self._show_upload_output.get_active()
		DarkTheme = self._dark_theme.get_active()
		EditorFont = self._editor_font.get_font_name()
		KeepOutputFiles = self._keep_output_files.get_active()
		
		LibrariesPath = self._libraries_path.get_filename()
		PackagesPath = self._packages_path.get_filename()
		BuilderPath = self._builder_path_file.get_filename()
		
		EditorTheme = self._editor_theme.get_active_text()
		
		#self._builder_path_file.
		self.set_value(self.setting_compile, ShowCompileOutput)
		self.set_value(self.setting_upload, ShowUploadOutput)
		self.set_value(self.dark_theme, DarkTheme)
		self.set_value(self.text_editor, EditorFont)
		self.set_value(self.keep_output, KeepOutputFiles)		
		
		self.set_value(self.libraries_paths, LibrariesPath)
		self.set_value(self.packages_path, PackagesPath)
		self.set_value(self.path_builder, BuilderPath)
		
		self.set_value(self.editor_theme, EditorTheme)
		
		self.close_preferences(None)
		App().Body.refresh_font()
		App().set_dark_theme()
		
		
	################# FILES HANDLER
	def create_settings_file(self):
		try:
			os.makedirs(self.dir_settings)
		except OSError as e:
			if e.errno != errno.EEXIST:
				raise
	
	def open_settings_file(self):
		if not os.path.isfile(self.dir_settings+self.file_name):
			self.create_settings_file()
		settings_obj = shelve.open(self.dir_settings+self.file_name)
		return settings_obj
	
	def default_config(self):
		obj_s = self.open_settings_file()
		obj_s[self.setting_compile] = False
		obj_s[self.setting_upload] = True
		obj_s[self.dark_theme] = True
		obj_s[self.text_editor] = "Monospace 11"
		obj_s[self.keep_output] = False
		obj_s[self.libraries_paths] = GLib.get_home_dir()+"/.ardunome/libraries"
		obj_s[self.packages_path] = GLib.get_home_dir()+"/.ardunome/hardware_packages/packages"
		obj_s[self.path_builder] = GLib.get_home_dir()+"/.ardunome"
		obj_s[self.examples_path] = [GLib.get_home_dir()+"/.ardunome/examples"]
		obj_s[self.editor_theme] = "oblivion"
		obj_s[self.repository_url] = ["http://downloads.arduino.cc/libraries/library_index.json", "http://downloads.arduino.cc/packages/package_index.json"]
		obj_s.close()
	
	def get_value(self, type):
		obj_s = self.open_settings_file()
		try:
			setting = obj_s[type]
		except:
			#TODO: put default particular config
			setting = None
		obj_s.close()
		return setting
	
	def set_value(self, type, value):
		obj_s = self.open_settings_file()
		obj_s[type] = value
		obj_s.close()
		
		
		
