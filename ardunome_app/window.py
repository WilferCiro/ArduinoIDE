'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Graphic library
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GtkSource', '3.0')
from gi.repository import Gtk, Gdk, Gio, GLib, Pango, Atk

# Translation
import locale
import gettext
import time
_ = gettext.gettext

# Functions
try:
	from functions import App, return_ui_file
	from text_editor import text_editor
	from directories_files import directories
except:
	from ardunome_app.functions import App, return_ui_file
	from ardunome_app.text_editor import text_editor
	from ardunome_app.directories_files import directories

# TODO: add all autocomplete commands
# TODO: empty widgets when are used

from ardunome_app.serial_window import serial_window

class MainWindow(Gtk.Bin):

	def close_app(self,*args):
		Gtk.main_quit(*args)

	def run(self):
		try:
			Gtk.Bin.__init__(self)
			self.builder = Gtk.Builder()
			path = return_ui_file("main.ui")
			
			# Add Glade File
			self.builder.add_from_file(path)
			
			# Start Window and propierties
			self.window_Main = self.builder.get_object("ArduinoideWindow")
			self.window_Main.connect("delete_event", self.close_app)
			self.window_Main.set_position(Gtk.WindowPosition.CENTER)
			self.window_Main.set_default_size(1030, 630)
			self.window_Main.maximize()
			
			#self.window_Main.connect('size-allocate', self.resized)
			
			self.application = App()
						
			# Current ID
			self.__current_id = 0
			
			# Files handler
			self.__dir = directories()

			# text view
			self.__text_view = text_editor()
			
			# tabwindow
			self.__principal_tab = self.builder.get_object('PannedBody')
			
			# Body Class			
			App().Body_camp = self.builder.get_object('BodyFull')
			App().Body_camp.pack_end(App().Body, True, True, True)
			self.__panned_left_panel = self.builder.get_object('PannedLeftPanel')
			
			# Left Panel Class		
			self.__left_panel_camp = self.builder.get_object('LeftPanelBoxFull')
			self.__left_panel_camp.pack_end(App().leftPanel, True, True, True)
			
			# Console panel		
			self.__console_panel_camp = self.builder.get_object('BoxConsoleFull')
			self.__console_panel_camp.pack_end(App().consolePanel, True, True, 0)
			
			# HeaderBar
			self.window_Main.set_titlebar(App().headerBar)
			
			# Serial ports
			self.serial_open = False
			
			# Shortcuts	
			# TODO add all shortcuts
			accel = Gtk.AccelGroup()
			accel.connect(Gdk.keyval_from_name('S'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('R'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('U'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('F'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('J'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('N'), Gdk.ModifierType.CONTROL_MASK, 0, self.on_accel_pressed)
			accel.connect(Gdk.keyval_from_name('F2'), 0, 0, self.on_accel_pressed)
			self.window_Main.add_accel_group(accel)

			self.window_Main.show_all()
			Gtk.main()			

		except KeyboardInterrupt:
			print ("\nExit on user cancel.")
			sys.exit(1)
	
	def resized(self, container, other):
		# FIXME: search a better signal to resize
		if container.get_size().width <= 750:
			App().Body.reorder_tabs(Gtk.PositionType.LEFT)
		else:
			App().Body.reorder_tabs(Gtk.PositionType.TOP)
	
	def on_accel_pressed(self, accel_group, acceleratable, keyval, modifier):
		if Gdk.ModifierType.CONTROL_MASK == modifier:
			if Gdk.keyval_name(keyval) == "s":
				App().Body.save_project()
			if Gdk.keyval_name(keyval) == "r":
				App().headerBar.on_compile(None)
			if Gdk.keyval_name(keyval) == "u":
				App().headerBar.on_upload(None)
			if Gdk.keyval_name(keyval) == "f":
				App().headerBar.on_search(None)
			if Gdk.keyval_name(keyval) == "j":
				App().Preferences.start()
			if Gdk.keyval_name(keyval) == "n":			
				App().headerBar.short_new_file()
		elif Gdk.keyval_name(keyval) == "F2":
			App().headerBar.on_rename_file(None)
	
	def create_project(self, path, name):
		if self.__dir.create_directories(path, name):
			self.__current_id = self.__current_id + 1
			self.__dir.create_file(path, name+".ino", True)
			App().headerBar.add_project(path, name, self.__current_id)
			App().Body.add_project(self.__current_id, path, name, True)
		else:
			dialog = Gtk.MessageDialog(self.window_Main, 0, Gtk.MessageType.INFO,
				Gtk.ButtonsType.OK, "Information!")
			dialog.format_secondary_text("This project exists.")
			dialog.run()
			dialog.destroy()
	
	def open_project(self, path, name, is_ino_pde):
		if not App().Body.is_open_project(path, name):
			self.__current_id = self.__current_id + 1
			self.__dir.create_directories(path, name)
			App().headerBar.add_project(path, name, self.__current_id)
			App().Body.add_project(self.__current_id, path, name, False)
		else:
			dialog = Gtk.MessageDialog(self.window_Main, 0, Gtk.MessageType.INFO,
				Gtk.ButtonsType.OK, "Information!")
			dialog.format_secondary_text("This project is open.")
			dialog.run()
			dialog.destroy()
		
	def set_open_project(self, project_id):
		App().Body.set_open_project(project_id)
		
	def add_file(self, file_name, is_class):
		path = App().Body.return_current_path()
		file_name, extension = self.__dir.separe_name_extension(file_name)
		if is_class:
			self.__dir.create_file(path, file_name+".cpp", False)
			self.__dir.create_file(path, file_name+".h", False)
		else:
			self.__dir.create_file(path, file_name+".cpp", False)
		App().Body.add_file_project(path, file_name, is_class)			

	def close_project(self):
		App().Body.close_current_project()
		other_id = App().headerBar.close_current_project()		
		if other_id is not None:
			self.set_open_project(other_id)
		else:
			App().Body.show_initial_box()
		
	def open_serial(self):
		########## Serial Window
		if self.serial_open == False:
			self.serial_open = True
			__serial_window = serial_window()
			__serial_window.show_all()
	
	def function_close_serial(self):
		self.serial_open = False
		
	def left_panel_show_hide(self, position):
		self.__panned_left_panel.set_position(position)
		
		
