'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''


from gi.repository import Gtk, Gio

try:
	from functions import return_mime_type, APP_NAME, App
except:
	from ardunome_app.functions import return_mime_type, APP_NAME, App


import os
from operator import itemgetter
import datetime
import glob

class examples_manager:
	"""
		Init examples manager
	"""	
	def __init__(self):
		self.__manager = Gtk.RecentManager.get_default()
	
	def put_examples(self, listbox):
		default_examples_path = App().Preferences.get_value(App().Preferences.examples_path)
		directories = self.all_examples_subdir(default_examples_path[0])
		directories = sorted(directories)
		for rec in directories:
			folder = rec.split("/")[-1]
			row = Gtk.ListBoxRow()
			hbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=2)						
			label1 = Gtk.Label("", xalign=0)
			label1.set_markup("<b>"+folder+"</b>")
			label2 = Gtk.Label("", xalign=0)
			label2.set_markup("<small>"+rec+"</small>")
			separator = Gtk.HSeparator()			
			hbox.pack_start(label1, True, True, 0)
			hbox.pack_start(label2, True, True, 0)	
			hbox.pack_start(separator, True, True, 5)			
			row.add(hbox)
			row.show_all()
			listbox.insert(row, -1)
	
	def all_examples_subdir(self, path, set_recursive = False):
		search = glob.glob(path+"/**", recursive=set_recursive)
		return search

	def update_examples_view(self, listbox, filter = None):
		if filter is None:
			recents = self.return_recent_files()
		else:
			recents = self.return_recent_files_filter(filter)
		for rec in recents:
			row = Gtk.ListBoxRow()
			hbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=2)
			
			base = os.path.basename(rec["file"])
			path = os.path.dirname(rec["file"])
			dire = path.split(os.sep)
			parent_dir = dire[len(dire)-1]
			
			label1 = Gtk.Label(parent_dir, xalign=0)
			label1.set_markup("<b>"+parent_dir+"</b> : <small>"+datetime.datetime.fromtimestamp(int(rec["date"])).strftime('%Y-%m-%d %H:%M:%S')+"</small>")
			label2 = Gtk.Label("", xalign=0)
			label2.set_markup("<small>"+rec["file"]+"</small>")
			separator = Gtk.HSeparator()			
			hbox.pack_start(label1, True, True, 0)
			hbox.pack_start(label2, True, True, 0)	
			hbox.pack_start(separator, True, True, 5)			
			row.add(hbox)
			row.show_all()
			listbox.insert(row, -1)
		return recents

