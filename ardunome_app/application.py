'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''
# FIXME: recuperar memoria cuando se cierra un proyecto

try:
	from window import MainWindow
	from functions import return_css_file, VERSION, APP_NAME
	from console_panel import console
	from compiler import compileUpload
	from left_panel import left_panel
	from body import body
	from serial_io import visual_port
	from preferences import preferences
	from header_bar import header_bar
	from repository_window import repository_window
except:
	from ardunome_app.window import MainWindow
	from ardunome_app.functions import return_css_file, VERSION, APP_NAME
	from ardunome_app.console_panel import console
	from ardunome_app.compiler import compileUpload
	from ardunome_app.left_panel import left_panel
	from ardunome_app.body import body
	from ardunome_app.serial_io import visual_port
	from ardunome_app.preferences import preferences
	from ardunome_app.header_bar import header_bar
	from ardunome_app.repository_window import repository_window

import sys

from gi.repository import Gio, Gtk, Gdk, GLib

class App(Gtk.Application):
	
	def __init__(self):
		Gtk.Application.__init__(self,
				             application_id="org.gnome.ardunome",
				             flags=Gio.ApplicationFlags.FLAGS_NONE)

		self.connect("activate", self.activateCb)
		self.Preferences = preferences() # should be before of leftPanel
		self.VisualPort = visual_port() # should be before of leftPanel
		self.consolePanel = console()
		self.headerBar = header_bar()
		self.MainWindow = MainWindow()
		self.compileUpload = compileUpload()
		self.leftPanel = left_panel()
		self.Body = body()
		self.RepositoryWindow = repository_window()

		self.version = VERSION
				
		#css
		style_provider = Gtk.CssProvider()
		css = open(return_css_file(),'rb')
		css_data = css.read()
		css.close()
		style_provider.load_from_data(css_data)
		Gtk.StyleContext.add_provider_for_screen(
			Gdk.Screen.get_default(), style_provider,     
			Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
		)	
		
		# self.connect("command-line", self.my_argv)
		self.set_dark_theme()
	
	def activateCb(self, app):     
		"""window = Gio.ApplicationWindow()
		app.add_window(window)
		window.show()"""
		self.MainWindow.run()
	
	def set_dark_theme(self):
		is_dark = self.Preferences.get_value(self.Preferences.dark_theme)
		settings = Gtk.Settings.get_default()
		settings.set_property("gtk-application-prefer-dark-theme", is_dark)

def run():
	app = App()
	try:
		app.run(sys.argv)
	except KeyboardInterrupt:
		print ("Quitting")
		app.quit()       

if __name__ == '__main__':
    run()
#sys.exit(0)

