'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

from gi.repository import Gtk, Gio, Gdk, GLib

# Custom classes
try:
	from functions import return_ui_file, App
	from serial_io import serial_monitor
except:
	from ardunome_app.functions import return_ui_file, App
	from ardunome_app.serial_io import serial_monitor

USER = 1
MACHINE = 2

class serial_individual(Gtk.Bin):
	"""
        Init text editor
    """
	def __init__(self, history_box):
		"""
            Init source view
        """
		Gtk.Bin.__init__(self)	
		
		# Configure file
		path = return_ui_file("individual_serial.ui")
		self.builder = Gtk.Builder()
		self.builder.add_from_file(path)
		self.builder.connect_signals(self)
		self.__principal_box = self.builder.get_object("SerialBox")
		
		self.add(self.__principal_box)
		
		self.__serial_obj = serial_monitor()
		
		self.__history_box = history_box
		
		#### Visual Vars
		self._new_line = True
		self._autoscroll = True
		self._show_user_text = True
		self.colorUser = "red"
		self.colorMachine = "blue"
		
		#### Menu Options
		# Save attributes
		self.__save_attr_button = self.builder.get_object("SaveAttrButton")
		self.__save_attr_button.connect("clicked", self.save_att)
		# Save changes
		self.__save_changes_button = self.builder.get_object("SaveChangesButton")
		self.__save_changes_button.connect("clicked", self.save_changes)
		# Bauds
		self.__serial_conf_bauds = self.builder.get_object("SerialConfBauds")
		# Port
		self.__serial_conf_port = self.builder.get_object("SerialConfPort")		
		# Codification
		self.__serial_conf_cod = self.builder.get_object("SerialConfCod")		
		# New Line
		self.__serial_conf_new_line = self.builder.get_object("SerialConfNewLine")		
		# Auto Scroll
		self.__serial_conf_auto_scroll = self.builder.get_object("SerialConfAutoscroll")		
		# Show User
		self.__serial_conf_show_user = self.builder.get_object("SerialConfShowUser")		
		# button conf reset
		self.__serial_conf_reset = self.builder.get_object("SerialConfReset")	
		#self.__serial_conf_reset.connect("clicked", self.reset_board)	
		# Button clear console
		self.__serial_conf_clear_console = self.builder.get_object("SerialConfClearConsole")
		self.__serial_conf_clear_console.connect("clicked", self.clear_console)	
		# Export data
		self.__serial_conf_export = self.builder.get_object("SerialConfExport")
		self.__serial_conf_export.connect("clicked", self.export_data)
		# Parity
		self.__serial_conf_parity = self.builder.get_object("SerialConfParity")		
		# Stop Bits
		self.__serial_conf_stop_bit = self.builder.get_object("SerialConfStopBit")		
		# Number of bits
		self.__serial_conf_bits = self.builder.get_object("SerialConfBits")		
		
		##### Serial visual
		# Button send serial		
		self.__send_serial = self.builder.get_object("SendSerial")	
		self.__send_serial.connect("clicked", self.send_serial)	
		# Text Send Serial	
		self.__text_send_serial = self.builder.get_object("TextSendSerial")	
		self.__text_send_serial.connect("activate", self.send_serial)		
		
		# Output serial		
		self.__scroll_output = self.builder.get_object("ScrolledWindow")
		self.__output_serial = self.builder.get_object("OutputSerial")	
		# Notification conect		
		self.__conection_notification = self.builder.get_object("ConectionNotification")	
		# Color back
		self.__bg_color_chooser = self.builder.get_object("BGColor")
		# Send Button
		self.disable_send()
		self.show_all()
	
	def __del__(self):
		self.disconnect_port()
		del self
	
	def disable_send(self):
		self.__send_serial.set_sensitive(False)
		self.__text_send_serial.set_sensitive(False)
		self.__save_changes_button.set_label("Save changes and connect")
	
	def enable_send(self):
		self.__send_serial.set_sensitive(True)
		self.__text_send_serial.set_sensitive(True)
		self.__save_changes_button.set_label("Save changes and disconnect")
	
	def save_att(self, *args):
		# Visual Config
		self._new_line = self.__serial_conf_new_line.get_active_text()
		self._autoscroll = self.__serial_conf_auto_scroll.get_active()
		self._show_user_text = self.__serial_conf_show_user.get_active()
		
		color = self.__bg_color_chooser.get_color()	
		self.__output_serial.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA.from_color(color))
	
	def save_changes(self, *args):
		self.__conection_notification.set_text("Connecting and setting your config...")
		# Port config
		bauds = self.__serial_conf_bauds.get_active_text()
		port = self.__serial_conf_port.get_active_text()
		parity = self.__serial_conf_parity.get_active_text()
		stop_bit = self.__serial_conf_stop_bit.get_active()
		bits_number = self.__serial_conf_bits.get_active_text()
		
		# change_settings
		self.__serial_obj.set_bauds(bauds)
		self.__serial_obj.set_parity(parity)
		self.__serial_obj.set_stop_bits(stop_bit)
		self.__serial_obj.set_bits_number(bits_number)
		
		conected = self.__serial_obj.get_connected()
		if not conected:
			self.__serial_obj.close_connection()
			self.__conection_notification.set_text("Restarting connection")
			self.__serial_obj.set_port(port)
			conected = self.__serial_obj.start_connection()			
			if conected == True:
				self.__conection_notification.set_text("Connected to port: "+port+" - "+bauds)
				self.enable_send()
				GLib.timeout_add(10, self.read_serial)	
			else:
				error = self.__serial_obj.get_error()
				self.__conection_notification.set_text("No connected, "+error)
		else:
			self.disconnect_port()
	
	def disconnect_port(self):
		self.disable_send()
		self.__serial_obj.close_connection()
		self.__conection_notification.set_text("No connected, ")
	
	def read_serial(self):
		if self.__serial_obj.get_connected():
			text_read = self.__serial_obj.read()
			if text_read != None:
				self.set_text_console(str(text_read), MACHINE)
			GLib.timeout_add(20, self.read_serial)
		else:
			error = self.__serial_obj.get_error()
			self.__conection_notification.set_text("No connected, "+error)			
	
	def clear_console(self, *args):
		self.__output_serial.set_text("")
	
	def save_history(self, text):
		self.__history_box.set_text(self.__history_box.get_text()+"\n"+text.replace("\n",""))
	
	def set_text_console(self, text, type):		
		if type == USER:
			self.save_history(text)
			if self._show_user_text:
				self.__output_serial.set_markup(self.__output_serial.get_label()+"<span color='"+self.colorUser+"' font = 'Helvetica'>"+text+"</span>")
		else:
			self.__output_serial.set_markup(self.__output_serial.get_label()+"<span color='"+self.colorMachine+"' font = 'Helvetica'>"+text+"</span>")
		try:
			if self._autoscroll:
				adj = self.__scroll_output.get_vadjustment()
				adj.set_value(adj.get_upper() - adj.get_page_size())
		except:
			None
	def export_data(self, *args):
		dialog = Gtk.FileChooserDialog("Set the name of yout file", None,
			Gtk.FileChooserAction.SAVE,
			(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
			Gtk.STOCK_SAVE, Gtk.FileChooserAction.SAVE))
		self.add_filters(dialog)
		dialog.set_current_name("SerialOutput.txt")
		dialog.set_create_folders(True)
		response = dialog.run()
		if response == Gtk.FileChooserAction.SAVE:
			file = open(dialog.get_uri().replace("file://", ""), "w+")
			file.write(self.__output_serial.get_text())
			file.close()
		dialog.destroy()

	def add_filters(self, dialog):
		filter_text = Gtk.FileFilter()
		filter_text.set_name('Text files')
		filter_text.add_mime_type('text/plain')
		dialog.add_filter(filter_text)
	
	def send_serial(self, *args):
		text_to_send = self.__text_send_serial.get_text()
		if text_to_send != "":
			text_to_send = text_to_send + "\n"
			sent = self.__serial_obj.send_data(text_to_send)
			self.__text_send_serial.set_text("")
			self.set_text_console(text_to_send, USER)
			if sent == False:
				error = self.__serial_obj.get_error()
				self.__conection_notification.set_text(self.__conection_notification.get_text()+", "+error)
				
	def set_colors_console(self, colorUser, colorMachine):
		self.__output_serial.set_markup(
			self.__output_serial.get_label().replace(self.colorUser, colorUser).replace(self.colorMachine, colorMachine))	
		self.colorUser = colorUser
		self.colorMachine = colorMachine
		
