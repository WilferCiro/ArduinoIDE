'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''
# Required Libraries
from gi.repository import Gtk

# Custom Widgets
try:
	from directories_files import directories
	from tabs import tabs
	from functions import get_global_font
except:
	from ardunome_app.directories_files import directories
	from ardunome_app.tabs import tabs
	from ardunome_app.functions import get_global_font
	
class notebook(Gtk.Notebook):
	"""
        Init text editor
    """
	def __init__(self, _id, _path, name, is_new):
		"""
            Init source view
        """
		Gtk.Notebook.__init__(self)		
		self._current_text_views = []
		self.__id = _id
		self.__path = _path
		self.__project_name = name
		self.__is_new = is_new
		
		self.__directories_class = directories()
		
		self.open_files_in_dir()
	
	def __del__(self):
		for text in self._current_text_views:
			del text
		
	def open_files_in_dir(self):
		#self._current_text_views.empty()
		for file in self.__directories_class.files_directory(self.__path):
			self.add_tab(file)
		self.expand_childs()
	
	def remove_tab(self, tab):
		try:
			self.remove(tab)
		except:
			None
	
	def open_file(self, file_name):
		self.add_tab(file_name)
		self.expand_childs()
	
	def add_tab(self, file):
		# Label
		title = Gtk.Label(file)
		if self.__is_new:
			tab = tabs(self.__path, self.__project_name+".ino", title)
		else:
			tab = tabs(self.__path, file, title)				
		# Add Tab with options
		current_page_number = self.append_page(tab, title)	
		current_page = self.get_nth_page(current_page_number)
		current_page.modify_font(get_global_font())
		self.show_all()
		self._current_text_views.append(tab)
	
	def expand_childs(self):
		try:
			i = 0
			while self.get_children()[i]:	
				self.child_set_property(self.get_children()[i], "tab-expand", True)
				self.child_set_property(self.get_children()[i], "reorderable", True)
				i = i+1
		except:
			print("")
	
	def refresh_font(self):
		for tab in self._current_text_views:
			tab.modify_font(get_global_font())
			tab.put_scheme()
	
	def save_project(self):
		for tab in self._current_text_views:
			tab.save_page()
	
	def return_project_id(self):
		return self.__id
	
	def return_path(self):
		return self.__path
	
	def return_principal_file(self):
		return self.__path+"/"+self.__project_name+".ino", self.__path
	
	def return_project_name(self):
		return self.__project_name
	
	def get_page(self):
		page = self.get_current_page()
		return self.get_children()[page]
	
	def remove_pages(self):
		try:
			while self.get_children()[0]:
				self.remove(self.get_children()[0])
		except:
			None
		self._current_text_views = []
	
	def reload_files(self):
		self.remove_pages()
		self.open_files_in_dir()
	
	def return_files_project(self):
		files = []
		for te in self._current_text_views:
			files.append(te.return_path_name())
		return files
	
