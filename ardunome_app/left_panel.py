'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''


from gi.repository import Gtk, Gio, GLib

try:
	from functions import return_ui_file, App
	from boards import boardsHandler
	from programmers import ProgrammersHandler
except:
	from ardunome_app.functions import return_ui_file, App
	from ardunome_app.boards import boardsHandler
	from ardunome_app.programmers import ProgrammersHandler

#TODO: Burn Bootloader
#TODO: libraries of projects
#TODO: example projects
#TODO: add thread when open and save project
#TODO: finish serial monitor, check time when read and bauds, reload ports
#TODO: finish preferences

class left_panel(Gtk.Bin):
	"""
        Init text editor
    """
	def __init__(self):
		"""
            Init source view
        """
		Gtk.Bin.__init__(self)
		
		# Configure file
		path = return_ui_file("left_panel.ui")
		self.builder = Gtk.Builder()
		self.builder.add_from_file(path)
		self.builder.connect_signals(self)
		
		self.add(self.builder.get_object("LeftPanelBox"))
		
		# Visual Objects
		self.__box_serial = self.builder.get_object("BoxSerialPort")
		self.__box_serial.connect("changed", self.change_port)
		self.__box_baud = self.builder.get_object("BoxBaud")
		
		# Serial object
		self._boards_object = boardsHandler()
		self._program_object = ProgrammersHandler()
		
		################# Left Panel Menu
		# LibrariesMenuAction
		self._button_libraries = self.builder.get_object("LibrariesMenuAction")
		self._button_libraries.connect("clicked", self._libraries_window)
		# Board Information
		self._board_preferences = self.builder.get_object("MicroInformationMenuAction")
		self._board_preferences.connect("clicked", self._menu_board_information)		
		# About Button
		self._button_about = self.builder.get_object("AboutMenuAction")
		self._button_about.connect("clicked", self._menu_about)
		# Edit Menu
		self._button_close_project = self.builder.get_object("CloseProjectMenuAction")
		self._button_close_project.connect("clicked", self._close_project)
		# Save As...	
		self._button_save_copy = self.builder.get_object("SaveAsMenuAction")
		self._button_save_copy.connect("clicked", self._save_copy)
		# Open Reference page
		self._button_open_webpage = self.builder.get_object("WebPageMenuAction")
		self._button_open_webpage.connect("clicked", self._open_page)
		# Open Reference page
		self._button_open_reference = self.builder.get_object("ReferenceMenuAction")
		self._button_open_reference.connect("clicked", self._open_reference)
		
		##### About Window
		self._about_window = self.builder.get_object("AboutWindow")
		self._about_window.connect("delete_event", self._close_about)
		
		## Boards		
		self.__list_box_boards = self.builder.get_object("BoxBoards1")
		self.__list_box_boards.connect("row-activated", self.__on_board_activated)
		self.get_boards()
		self.__list_box_boards.select_row(self.__list_box_boards.get_row_at_index(1))
		self.__on_board_activated(None)
		self.__search_board_input = self.builder.get_object("InputSearchBoard")		
		self.__search_board_input.connect("search-changed", self.__on_board_search)
		
		## Programmers
		self.__list_box_programmers = self.builder.get_object("BoxProgrammers")
		self.__list_box_programmers.connect("row-activated", self.__on_programmer_activated)
		self.get_programmers()
		self.__list_box_programmers.select_row(self.__list_box_programmers.get_row_at_index(1))
		self.__on_programmer_activated(None)
		self.__search_board_input = self.builder.get_object("InputSearchProgrammers")
		self.__search_board_input.connect("search-changed", self.__on_programmers_search)
				
		# Init 
		self.get_serials()
		self.__box_serial.set_active(0)
	
	def __on_board_activated(self, *args):
		selected = self.__list_box_boards.get_selected_row()
		box = selected.get_children()[0]
		path = box.get_children()[1].get_text()
		id_board = box.get_children()[0].get_text().split(" - ")[1]
		name_board = box.get_children()[0].get_text().split(" - ")[0]
		if id_board != "" and id_board != None:
			App().consolePanel.set_preview_board(name_board)
		
	def __on_programmer_activated(self, *args):
		selected = self.__list_box_programmers.get_selected_row()
		box = selected.get_children()[0]
		path = box.get_children()[1].get_text()
		id_program = box.get_children()[0].get_text().split(" - ")[1]
		name_program = box.get_children()[0].get_text().split(" - ")[0]
		if id_program != "" and id_program != None:
			App().consolePanel.set_preview_programmer(name_program)
	
	def get_upload_board(self):
		selected = self.__list_box_boards.get_selected_row()
		box = selected.get_children()[0]
		path = box.get_children()[1].get_text()
		id_board = box.get_children()[0].get_text().split(" - ")[1]
		upload_options = None
		build_options = None
		bootloader_options = None
		if id_board != "" and id_board != None:
			bootloader_options, upload_options, build_options = self._boards_object.setPreferencesBoard(id_board, path)
		return upload_options, build_options
	
	def get_path_board(self):
		selected = self.__list_box_boards.get_selected_row()
		box = selected.get_children()[0]
		path = box.get_children()[1].get_text()
		id_board = box.get_children()[0].get_text().split(" - ")[1]
		mini_board = self._boards_object.get_path_mini(path)+":"+id_board
		print(mini_board)
		return mini_board
		
	def change_port(self, *args):
		name = str(self.get_active_port())
		App().consolePanel.set_preview_port(name)
		
	def get_serials(self):
		ports = App().VisualPort.scan()
		if ports != None:
			actual = self.__box_serial.get_active_text()
			self.__box_serial.remove_all()
			index = 0
			for pt in ports:
				self.__box_serial.append_text(pt)
				if actual == pt:
					self.__box_serial.set_active(index)
				index = index + 1
		GLib.timeout_add(4000, self.get_serials)			
	
	def __on_board_search(self, input):
		text = input.get_text()
		self.get_boards(text)
		
	def get_boards(self, filter = None):
		returned_boards = self._boards_object.returnBoards()				
		index = 0		
		try:
			while self.__list_box_boards.get_children()[0]:
				self.__list_box_boards.remove(self.__list_box_boards.get_children()[0])
		except:
			None		
		for bd in returned_boards:
			if (filter != None and (filter.lower() in bd["name"].lower() or filter.lower() in bd["id"].lower())) or filter == None:
				row = Gtk.ListBoxRow()
				hbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=2)
						
				label1 = Gtk.Label("", xalign=0)
				label1.set_markup("<b>"+bd["name"]+"</b> - "+bd["id"])
				label2 = Gtk.Label("", xalign=0)
				label2.set_markup("<small>"+bd["filename"]+"</small>")			
				separator = Gtk.HSeparator()
				hbox.pack_start(label1, True, True, 0)
				hbox.pack_start(label2, True, True, 0)	
				hbox.pack_start(separator, True, True, 5)			
				row.add(hbox)
				row.show_all()
				self.__list_box_boards.insert(row, -1)
	
	def __on_programmers_search(self, input):
		text = input.get_text()
		self.get_programmers(text)
	
	def get_programmers(self, filter = None):
		returned_programmers = self._program_object.returnProgrammers()
		index = 0
		try:
			while self.__list_box_programmers.get_children()[0]:
				self.__list_box_programmers.remove(self.__list_box_programmers.get_children()[0])
		except:
			None
		for bd in returned_programmers:
			if (filter != None and (filter.lower() in bd["name"].lower() or filter.lower() in bd["id"].lower())) or filter == None:
				row = Gtk.ListBoxRow()
				hbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=2)
						
				label1 = Gtk.Label("", xalign=0)
				label1.set_markup("<b>"+bd["name"]+"</b> - "+bd["id"])
				label2 = Gtk.Label("", xalign=0)
				label2.set_markup("<small>"+bd["filename"]+"</small>")			
				separator = Gtk.HSeparator()
				hbox.pack_start(label1, True, True, 0)
				hbox.pack_start(label2, True, True, 0)	
				hbox.pack_start(separator, True, True, 5)			
				row.add(hbox)
				row.show_all()
				self.__list_box_programmers.insert(row, -1)
		
	def _libraries_window(self, *args):
		App().RepositoryWindow.start()
	
	def _close_project(self, *args):
		App().MainWindow.close_project()
	
	def _menu_about(self, *args):
		self._about_window.run()
	
	def _menu_about(self, *args):
		self._about_window.run()
	
	def _close_about(self,*args):
		self._about_window.hide()
	
	def _save_copy(self, *args):
		App().Body.save_copy()
	
	def _open_reference(self, *args):		
		Gtk.show_uri(None, "https://www.arduino.cc/reference/en/", 100)
	
	def _open_page(self, *args):		
		Gtk.show_uri(None, "https://www.arduino.cc", 100)
	
	def get_active_port(self):
		actual = self.__box_serial.get_active_text()
		return actual
	
	def _menu_board_information(self, *args):
		tools_button = self.builder.get_object("ToolsButtonMenu")
		modal = self.builder.get_object("MenuProperties")
		label = self.builder.get_object("LabelPorperties")		
		port = self.get_active_port()		
		text = App().VisualPort.get_info_port(port)
		label.set_markup(text)
		modal.set_relative_to(tools_button)
		modal.show()
	
		
