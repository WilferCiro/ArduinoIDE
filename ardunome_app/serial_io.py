'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''


import glob

import time
import serial

import gettext
_ = gettext.gettext

from serial.tools import list_ports

class visual_port:
	'''
		Init class	
	'''	
	def __init__(self):
		self.current_ports = []
		
	def scan(self):
		"""scan for available ports. return a list of device names."""
		ports = []
		ports_class = list_ports.comports()
		for i in ports_class:
			ports.append(i.device)
		if self.current_ports != ports:
			self.current_ports = ports
			return ports
		else:
			return None
	
	def get_info_port(self, port_name):
		return_text = ""
		ports_class = list_ports.comports()
		for i in ports_class:
			if i.device == port_name:
				#try:
				return_text = "<b>Description: </b>"+str(i.description)+\
							"\n<b>Name: </b>"+str(i.name)+\
							"\n<b>Serial Number: </b>"+str(i.serial_number)+\
							"\n<b>Location</b> :"+str(i.location)+\
							"\n<b>Manufacturer</b> :"+str(i.manufacturer)+\
							"\n<b>Product</b> :"+str(i.product)+\
							"\n<b>Interface</b> :"+str(i.interface)+\
							"\n<b>Pid</b> :"+str(i.pid)+\
							"\n<b>Vid</b> :"+str(i.vid)
		return return_text
	
	def verify_port(self, port_name):
		verified = False
		error_txt = ""
		try:
			s = serial.Serial(port_name)
			s.close()
			verified = True
		except Exception as inst:
			error_txt = str(inst)
			verified = False
		return verified, error_txt

class serial_monitor:
	def __init__(self):
		"""9600 8N1"""
		self.serial = serial.Serial(
			port=None,
			baudrate=9600,
			parity=serial.PARITY_NONE,
			stopbits=serial.STOPBITS_ONE,
			bytesize=serial.EIGHTBITS
		)
		self.connected = False
		self.error_txt = ""

	def __del__(self):
		self.serial.close()
	
	def get_connected(self):
		return self.connected
	
	def start_connection(self):
		try:
			self.serial.open()
			self.error_txt = ""
			self.connected = True
		except Exception as inst:
			self.error_txt = str(inst)
			self.connected = False
		return self.connected
	
	def get_error(self):
		return self.error_txt
	
	def close_connection(self):
		self.serial.close()
		self.connected = False

	def read(self):
		try:
			if self.serial.inWaiting() > 0:
				text = self.serial.read(self.serial.inWaiting()).decode(errors='ignore')
				return text
			return None
		except:
			return None
	
	def get_bauds(self):
		return self.serial.baudrate
	
	def get_parity(self):
		return self.serial.parity

	def get_stop_bits(self):
		return self.serial.stopbits
	
	def get_port(self):
		return self.serial.port
	
	def set_bauds(self, bauds):
		self.serial.baudrate = bauds
	
	def set_parity(self, parity):
		if parity == "Odd":
			self.serial.parity = serial.PARITY_ODD
		elif parity == "Even":
			self.serial.parity = serial.PARITY_EVEN		
		else:
			self.serial.parity = serial.PARITY_NONE

	def set_stop_bits(self, stopbits):
		if stopbits == True:
			self.serial.stopbits = serial.STOPBITS_TWO
		else:
			self.serial.stopbits = serial.STOPBITS_ONE
		
	def set_bits_number(self, number):
		if number == "7":
			self.serial.bytesize = serial.SEVENBITS
		elif number == "6":
			self.serial.bytesize = serial.SIXBITS		
		else:
			self.serial.bytesize = serial.EIGHTBITS			
	
	def set_port(self, port):
		self.serial.port = port
	
	def send_data(self, data):
		sent = False
		try:
			self.serial.write(data.encode())
			self.error_txt = ""
			sent = True
		except Exception as inst:
			self.error_txt = str(inst)
		return sent

	def getBaudrates(self):
		return [200, 375, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200]

