'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

from gi.repository import Gtk, Gio, Gdk

try:
	from serial_box import serial_individual
	from functions import return_ui_file, App
except:
	from ardunome_app.serial_box import serial_individual
	from ardunome_app.functions import return_ui_file, App
	
class serial_window(Gtk.Bin):
	"""
        Init text editor
    """
	def __init__(self):
		"""
            Init source view
        """
		Gtk.Bin.__init__(self)
		
		# Configure file
		path = return_ui_file("serial_window.ui")
		self.builder = Gtk.Builder()
		self.builder.add_from_file(path)
		self.builder.connect_signals(self)
		self.__window = self.builder.get_object("SerialWindow")
		self.__window.connect("delete_event", self.close_serial)
		self.__window.set_position(Gtk.WindowPosition.CENTER)
		self.__window.set_default_size(1030, 630)	
		
		# New Buttons
		self.__new_serial_button = self.builder.get_object("AddSerialBox")
		self.__new_serial_button.connect("clicked", self.new_serial)
		
		# Box history
		self.__history_box = self.builder.get_object("HistoryBox")
		
		# Actions Buttons  
		self.__button_all_consoles = self.builder.get_object("ClearAllConsoles")
		self.__button_clear_history = self.builder.get_object("ClearHistory")
		self.__button_clear_history.connect("clicked", self.__clear_history)
		self.__button_all_consoles.connect("clicked", self.__clean_consoles)
		# Color Buttons
		self.__user_color = self.builder.get_object("UserColor")
		self.__machine_color = self.builder.get_object("MachineColor")
		self.__save_prefs = self.builder.get_object("SaveGeneralPrefs")
		self.__save_prefs.connect("clicked", self.save_preferences)
		# Serials list
		self.__box_serials = self.builder.get_object("BoxSerials")
		self.__serial_list = []
		self.new_serial(None)
		self.add(self.__window)
		
		# Initial Config
		self.save_preferences(None)
	
	def __clean_consoles(self, *args):
		for port in self.__serial_list:
			port.clear_console(None)			
			
	def __clear_history(self, *args):
		self.__history_box.set_text("")
	
	def save_preferences(self, *args):
		colorUser = self.rgba_to_hex(Gdk.RGBA.from_color(self.__user_color.get_color()))
		colorMachine = self.rgba_to_hex(Gdk.RGBA.from_color(self.__machine_color.get_color()))
		for port in self.__serial_list:
			port.set_colors_console(colorUser, colorMachine)
	
	def rgba_to_hex(self, color):  
		"""Return hexadecimal string for :class:`Gdk.RGBA` `color`."""  
		return "#{0:02x}{1:02x}{2:02x}".format(
			int(color.red  * 255),
			int(color.green * 255),
			int(color.blue * 255)
		) 
	
	def close_serial(self, *args):
		for port in self.__serial_list:
			port.disconnect_port()
		App().MainWindow.function_close_serial()
		del self
		
	def new_serial(self, *args):
		if len(self.__serial_list) < 3:
			serial = serial_individual(self.__history_box)
			self.__box_serials.pack_end(serial, True, True, True)
			self.__serial_list.append(serial)
			if len(self.__serial_list) == 3:
				self.__new_serial_button.set_sensitive(False)
				self.__new_serial_button.set_tooltip_text("Only can add 3 modules")
				
				
		
