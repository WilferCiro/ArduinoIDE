'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''
import os
from gi.repository import Gio

from send2trash import send2trash

class directories():
	"""
		Init Directories class
	"""
	#def __init__(self):
	#	print('directories')

	def create_directories(self, path, project_name):
		# mkdir path
		# mkdir path/project_name
		# mkdir path/project_name/Build
		# mkdir path/project_name/Data
		
		if os.path.exists(path):
			return False
		
		directories_create = [path, path+"/Build", path+"/Data"]
		for dir in directories_create:
			if not os.path.exists(dir):
				os.makedirs(dir)
		return True

	def save_file(self, path, file_name, content):
		# Save File
		filename, file_extension = os.path.splitext(file_name)
		if file_extension == None or file_extension !='':
			os.path.join(file_name + ".cpp")		
		file = open(path+"/"+file_name,"w")
		file.write(str(content))
		file.close()
			
	def create_file(self, path, file_name, is_initial):
		file = open(path+"/"+file_name,"w")
		
		if is_initial:
			file.write("//@Created with Ardunome - "+file_name+"\n\nvoid setup(){\n\t//Setup your code here\n\t\n}\nvoid loop(){\n\t//Main your code here\n\t\n}")
		else:
			name, extension = self.separe_name_extension(file_name)
			name = name.replace(' ','_')
			if extension == '.cpp':
				file.write("#include \""+name+".h\"\n\nvoid "+name+"::begin(){\n\t// Your begin code here\n}")
			elif extension == '.h':
				name_upper = name.upper()
				file.write("#ifndef "+name_upper+"_H\n#define "+name_upper+"_H\n\n#include <Arduino.h>\n#include <Wire.h>\nclass "+name+"{\npublic:\n\tvoid begin();\n\n};\n\n#endif")
		file.close()
	
	def return_file_name(self, rec):
		base = os.path.basename(rec)
		path = os.path.dirname(rec)
		dire = path.split(os.sep)
		parent_dir = dire[len(dire)-1]
		return parent_dir
	
	def separe_name_extension(self, file_name):
		file_name, file_extension = os.path.splitext(file_name)
		return file_name, file_extension
	
	def files_directory(self, path):
		files = []
		# read the entries
		with os.scandir(path) as listOfEntries:  
			for entry in listOfEntries:
				# print all entries that are files
				if entry.is_file():
					file_name, file_extension = os.path.splitext(entry)
					if file_extension == '.cpp' or file_extension == '.h' or file_extension == '.ino' or file_extension == '.c':
						files.append(entry.name)
		return files


	def on_save_project(self, path, name):
		if path == None or path == '' or not os.path.exists(path):
			print("Save as...")
		else:
			files = self.files_directory(path)
			
	def read_file(self, file):
		try:
			#f = gio.File(file)
			#stream = f.read()
			#text = stream.read()
			#stream.close()
			file = open(file,"r")
			text = file.read()
			file.close()
			del file
			return text
		except:
			print("Error reading file")
			return ""
	
	def delete_file(self, file):
		try:
			send2trash(file)
		except:
			os.remove(file)

	def rename_file(self, path, old_name, new_name):
		try:
			os.rename(path+"/"+old_name, path+"/"+new_name)
			return True
		except:
			return False
		
		
