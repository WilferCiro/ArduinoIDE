'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''


import subprocess
import tempfile
import time
import os
import glob
try:
	from functions import App, APP_NAME
except:
	from ardunome_app.functions import App, APP_NAME

class compileUpload():

	def __init__(self):
		print("Compiler start")
		self.project_dir = ""
		self.arduino_path = App().Preferences.get_value(App().Preferences.path_builder)
		self.builder_path = self.arduino_path+"/arduino-builder"
		self.pack_path = App().Preferences.get_value(App().Preferences.packages_path)

		self.hardware_paths = [
			self.arduino_path+"/hardware_packages",
			self.pack_path
		]

		self.tools_paths = [
			self.arduino_path+"/tools-builder",
			self.pack_path
		]

		self.libraries_paths = [
			App().Preferences.get_value(App().Preferences.libraries_paths)
		]

		self.IDE_VERSION = 10805
		self.process = None
		self.canceled = False
		self.current_compiled_path = ""
		self.prev_tmpdir = ""
	
	### COLORS:
	# relative things #215d9c
	# Errors #d01531
	# Accepts: #7ba722
	# Warnings: #f9c02f
	
	def showError(self, text):
		# TODO: verificar colores y errores
		has_error = False
		
		# Warnings
		text = text.replace("In file included from", "<span color='#f9c02f'>In file included from</span>")
		
		# Errors
		text = text.replace("error", "<span color='#d01531'>error</span>")
		text = text.replace("^", "<span color='#d01531'>^</span>")
		text = text.replace("exit status 1", "<span color='#d01531'>exit status 1</span>")
		if "error" in text or "exit status 1" in text:
			has_error = True
		# relative things
		text = text.replace("|--", "<span color='#7ba722'>"+(" -"*20)+"</span>")		
		text = text.replace("bytes", "<span color='#215d9c'>bytes</span>")
		text = text.replace("Compiler command:", "<span color='#215d9c'>Compiler command:</span>")
		
		# Uploading
		text = text.replace("avrdude done.  Thank you.", "<span color='#7ba722'>avrdude done.  Thank you.</span>")
		text = text.replace("avrdude: ", "<span color='#215d9c'>avrdude: </span>")
		text = text.replace("Writing", "<span color='#7ba722'>Writing</span>")
		text = text.replace("Reading", "<span color='#7ba722'>Reading</span>")
		if not self.canceled:
			App().consolePanel.set_text(text, False)
		return has_error
		
	def uploadCode(self):
		# TODO: verificar el tipo de uploader, avrdude, wiring, etc
		# TODO: verificar el mega.menu.
		hex_file = None
		port = str(App().leftPanel.get_active_port())
		
		free_port, error_txt = App().VisualPort.verify_port(port)
		if free_port == False:
			self.finisedAction()
			App().consolePanel.set_text("<span color='red'>"+error_txt+"</span>", False)
			return
		
		try:
			out_text, hex_file = self.compile(True)
			if self.process is not None or out_text == None:
				self.cancelCmd()
				return
		except:
			self.cancelCmd()
			return
		build_options, upload_options = App().leftPanel.get_upload_board()
		if upload_options == None:			
			self.cancelCmd()
			return
		
		uploader_prog = upload_options['tool']
			
		App().consolePanel.set_action("Uploading...")			
		show_upload_verbose = App().Preferences.get_value(App().Preferences.setting_upload)
		
		try:
			upload_cmd = []
			if uploader_prog == "bossac":
				bossac_path = self.pack_path+"/arduino/tools/bossac/1.6.1-arduino/bossac"
				upload_cmd.append(bossac_path)
				upload_cmd.append(" -i -d")
				upload_cmd.append(" --port="+port.replace("/dev/",""))
				upload_cmd.append(" -U false -e -w -b")
				if show_upload_verbose:
					upload_cmd.append(" -v") 
				upload_cmd.append(" "+hex_file+" -R")				
				
			elif uploader_prog == "avrdude":
				
				search = glob.glob(self.arduino_path+'/hardware_packages/tools/**/avrdude', recursive=True)
				search_conf = glob.glob(self.arduino_path+'/hardware_packages/tools/**/avrdude.conf', recursive=True)
				if len(search) == 0 or len(search_conf) == 0:
					print("No avrdude or avrdude.conf found on "+self.arduino_path+'/hardware_packages/tools/**')		
					self.cancelCmd("No avrdude found")
				else:
					print("Uploaders Founds: (avrdude)")
					print(search)
					print(search_conf)

				avrdude_path = search[0]
				avrdude_conf_path = search_conf[0]
				
				upload_cmd.append(avrdude_path)
				upload_cmd.append(" -C"+avrdude_conf_path)
				if show_upload_verbose:
					upload_cmd.append(" -v")
				upload_cmd.append(" -p"+build_options['mcu']+" -c"+build_options['core'])
				upload_cmd.append(" -P"+port)
				upload_cmd.append(" -b"+upload_options['speed'])
				upload_cmd.append(" -D -Uflash:w:"+hex_file+":i")
			
			cmdline = "".join(upload_cmd)
			text = out_text+ "\n\n|--\n" +"\nUpload command:\n"+cmdline + "\n\n|--\n"
			text = text + self.excecuteCmd(cmdline)
			
			self.showError(text)
			self.finisedAction()
		except:
			print("Error")
		
	def compile(self, upload_previus = False):
		self.canceled = False
		# Extract configs
		App().consolePanel.set_action("Compiling...")
		App().consolePanel.set_text("Compiling... wait please\n\n", False)
		try:
			rute_builder = App().leftPanel.get_path_board()
			file_to_compile, path_principal_file = App().Body.return_principal_file()
		except:
			self.cancelCmd()
			return	
			
		if self.process is not None:
			self.cancelCmd()
			return	
		
		keep_output = App().Preferences.get_value(App().Preferences.keep_output)
		if not keep_output:
			if self.current_compiled_path != file_to_compile:
				tmpdir = tempfile.mkdtemp(APP_NAME)
				self.prev_tmpdir = tmpdir
			else:
				tmpdir = self.prev_tmpdir
			build_path = tmpdir
			build_cache_path = tmpdir
		else:
			build_path = path_principal_file+"/Build"
			build_cache_path = path_principal_file+"/Data"
		
		if build_path == "" or build_path == None:
			tmpdir = tempfile.mkdtemp(APP_NAME)
			self.prev_tmpdir = tmpdir
			build_path = tmpdir
		if not os.path.exists(build_path):
			os.makedirs(build_path)
		if not os.path.exists(build_cache_path):
			os.makedirs(build_cache_path)
		
		get_warnings = "none"
		self.current_compiled_path = file_to_compile
		
		## Config Board
		build_options, upload_options = App().leftPanel.get_upload_board()
		if upload_options == None:			
			self.cancelCmd()
			return		
		uploader_prog = upload_options['tool']
		
		# Compilers Paths
		compilers_paths = None
		type_out = ""
		if uploader_prog == "bossac":
			compilers_paths = [
				"runtime.tools.bossac.path="+self.pack_path+"/arduino/tools/bossac/1.6.1-arduino"
				"runtime.tools.arm-none-eabi-gcc.path="+self.pack_path+"/arduino/tools/arm-none-eabi-gcc/4.8.3-2014q1"
			]
			type_out = "bin"
		elif uploader_prog == "avrdude":	
			search_gcc = glob.glob(self.arduino_path+'/hardware_packages/tools/**/bin/avr-gcc', recursive=True)
			search_avrdude = glob.glob(self.arduino_path+'/hardware_packages/tools/**/avrdude', recursive=True)
			search_OTA = glob.glob(self.arduino_path+'/hardware_packages/tools/**/arduinoOTA', recursive=True)
			if len(search_OTA) + len(search_avrdude) + len(search_gcc) < 3:
				print("No avrdude or avr-gcc or arduinoOTA found on "+self.arduino_path+'/hardware_packages/tools/**')
				self.cancelCmd("No avrdude or avr-gcc or arduinoOTA found")
			else:
				print("Paths of tools founds")
				print(search_gcc)
				print(search_avrdude)
				print(search_OTA)
				path_cc = os.path.dirname(os.path.dirname(search_gcc[0]))
			compilers_paths = [
				#""""runtime.tools.avr-gcc.path="+self.pack_path+"/arduino/tools/avr-gcc/4.9.2-atmel3.5.4-arduino2",
				#"runtime.tools.avrdude.path="+self.pack_path+"/arduino/tools/avrdude/6.3.0-arduino9 ",
				#"runtime.tools.arduinoOTA.path="+self.pack_path+"/arduino/tools/arduinoOTA/1.1.1""""
				
				"runtime.tools.avr-gcc.path="+path_cc,
				"runtime.tools.avrdude.path="+os.path.dirname(search_avrdude[0]),
				"runtime.tools.arduinoOTA.path="+os.path.dirname(search_OTA[0])
			]
			type_out = "hex"
		# Verbose
		show_compile_verbose = App().Preferences.get_value(App().Preferences.setting_compile)
		
		# Start Command
		compile_cmd = []
		# Builder Path - program
		compile_cmd.append(self.builder_path)
		# Opciones de compilación, and logger = human
		#compile_cmd.append(" -dump-prefs -logger=humantags")
		compile_cmd.append(" -compile -logger=humantags")
		
		for hard in self.hardware_paths:
			if hard != None:
				compile_cmd.append(" -hardware "+hard)
		for tool in self.tools_paths:
			if tool != None:
				compile_cmd.append(" -tools "+tool)
		for lib in self.libraries_paths:
			if lib != None:
				compile_cmd.append(" -libraries "+lib)

		compile_cmd.append(" -fqbn="+rute_builder+" -ide-version="+str(self.IDE_VERSION))
		compile_cmd.append(" -build-path "+build_path)
		compile_cmd.append(" -warnings="+get_warnings)
		compile_cmd.append(" -build-cache "+build_cache_path)
		compile_cmd.append(" -prefs=build.warn_data_percentage=75")
		
		for comp in compilers_paths:
			compile_cmd.append(" -prefs="+comp)
			
		if show_compile_verbose == True:	
			compile_cmd.append(" -verbose")
		compile_cmd.append(" "+file_to_compile)
		
		separate_file = file_to_compile.split("/")
		
		cmdline = "".join(compile_cmd)
		out = "Compiler command: \n"
		out = out + cmdline + "\n\n|--\n";
		out = out + self.excecuteCmd(cmdline)
		hex_file_final = build_path+"/"+separate_file[-1]+"."+type_out
		out = out+"<span color='green'><b>"+type_out+" file is alocated in: </b></span>"+hex_file_final+"\n"

		
		has_error = self.showError(out)			
		if has_error:
			self.finisedAction()
			return None, None
				
		if not upload_previus:
			self.finisedAction()
		
		return out, hex_file_final
	
	def finisedAction(self):
		App().consolePanel.set_action(None)	
		App().headerBar.change_icons(False)		
	
	def cancelCmd(self, text = ""):
		self.canceled = True
		if self.process is not None:
			self.process.terminate()
		self.process = None
		file = open('test.log', 'w+')
		file.close()
		self.finisedAction()
		App().consolePanel.set_text("<span color='red'>Canceled operation!\n"+text+"</span>", True)
	
	def excecuteCmd(self, cmdline):
		sout = ""
		soutGeneral = ""
		number = 0		
		try:			
			self.process = subprocess.Popen(cmdline, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)			
			if self.process != None:
				for c in iter(lambda: self.process.stdout.read(1), b''):
					number = number + 1
					try:
						sout = sout + c.decode("utf-8")
						soutGeneral = soutGeneral + c.decode("utf-8")
					except:
						sout = sout
					if number > 50:
						number = 0
						if not self.canceled:
							App().consolePanel.set_text(sout, True)
							time.sleep(0.1)
						sout = ""
					if self.process == None:
						break
						
			if not self.canceled:
				time.sleep(0.5)
				App().consolePanel.set_text(sout+"\n", True)
			time.sleep(0.1)
						
		except subprocess.SubprocessError as e:
			raise RuntimeError('Popen failed: {0.__name__} {1}'.format(type(e), str(e))) from e
		
		self.process = None
		return soutGeneral

