'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

from gi.repository import Gtk, Gdk

import os
import threading
from shutil import copy2
from distutils.dir_util import copy_tree
# Files tab
try:
	from notebook import notebook
	from functions import App, return_ui_file
	from recent_manager import recent_manager
	from examples import examples_manager
except:
	from ardunome_app.notebook import notebook
	from ardunome_app.functions import App, return_ui_file
	from ardunome_app.recent_manager import recent_manager
	from ardunome_app.examples import examples_manager

	
class body(Gtk.Box):
	"""
        Init text editor
    """
	def __init__(self):
		"""
            Init source view
        """
        # Configure file
		self.builder = Gtk.Builder()
		Gtk.Box.__init__(self)
		# Recent Manager
		self.__recent_manager = recent_manager()
		self.__examples_obj = examples_manager()
		
		self.recents_files = []
		self.__current_notebooks = []
		self.__current_note = None
		self.show_initial_box()
	
	def show_initial_box(self):
		self.remove_childs()
		path = return_ui_file("initial_body.ui")
		self.builder.add_from_file(path)
		self.builder.connect_signals(self)
		initialBody = self.builder.get_object("InitialBody")
		self.pack_end(initialBody, True, True, True)
		self.__recent_listbox = self.builder.get_object("ListBoxRecentsBody")
		self.recents_files = self.__recent_manager.update_recent_files_view(self.__recent_listbox)
		self.__recent_listbox.connect("row-activated", self.__on_row_activated)
		
		
		menu = Gtk.Menu()
		item_menu = Gtk.MenuItem("Menu")
		menu.append(item_menu)
		item_menu.show()
		
		#TODO: add examples manager
		self.__examples_listbox = self.builder.get_object("ListBoxExamplesBody")
		self.__examples_listbox.connect("row-activated", self.__on_example_activated, menu)		
		self.__examples_obj.put_examples(self.__examples_listbox)
		
		DRAG_ACTION = Gdk.DragAction.COPY
		initialBody.drag_dest_set(Gtk.DestDefaults.ALL, [], DRAG_ACTION)
		initialBody.drag_dest_add_text_targets()
		initialBody.connect("drag-data-received", self.on_drag_data_received)
		App().headerBar.disable_icons(False)
	
	def __on_example_activated(self, listbox, row, widget):
		print("Activated")
		widget.popup_at_widget(row, Gdk.Gravity.CENTER , Gdk.Gravity.CENTER , None)
		pass
	
	def on_drag_data_received(self, widget, drag_context, x,y, data,info, time):
		(TARGET_ENTRY_TEXT, TARGET_ENTRY_PIXBUF) = range(2)
		if info == TARGET_ENTRY_TEXT:
			filename = data.get_text().replace("file://", "")
			base = os.path.basename(filename)
			file_name, file_extension = os.path.splitext(base)
			path = os.path.dirname(filename)		
			App().MainWindow.open_project(path, file_name, True)


	def remove_childs(self):
		App().headerBar.disable_icons(True)
		try:
			while self.get_children()[0]:
				self.remove(self.get_children()[0])
		except:
			None
	
	def is_open_project(self, path, name):
		is_open = False
		for note in self.__current_notebooks:
			if note.return_principal_file() == path+"/"+name+".ino":
				is_open = True
		return is_open
		
	def add_project(self, id, path, name, is_new):
		self.remove_childs()
		note = notebook(id, path, name, is_new)
		self.__current_note = note
		self.__current_notebooks.append(note)
		self.pack_end(note, True, True, False)
		
	def set_open_project(self, project_id):
		for note in self.__current_notebooks:
			if note.return_project_id() == project_id:
				self.remove_childs()
				self.pack_end(note, True, True, False)
				self.__current_note = note
				return
				
	def add_file_project(self, path, file_name, is_class):
		if is_class:
			self.__current_note.open_file(file_name+".cpp")
			self.__current_note.open_file(file_name+".h")
		else:
			self.__current_note.open_file(file_name+".cpp")
	
	def return_current_path(self):
		return self.__current_note.return_path()
				
	def close_current_project(self):
		for note in self.__current_notebooks:
			if note.return_project_id() == self.__current_note.return_project_id():
				self.__current_notebooks.remove(note)
		if len(self.__current_notebooks) == 0:
			self.__current_note = None
	
	def save_project(self):
		if self.__current_note != None:
			hilo1 = threading.Thread(target=self.__current_note.save_project)
			hilo1.start()
			#self.__current_note.save_project()	
	
	def save_copy(self):
		principal_name = self.__current_note.return_project_name()
		dialog = Gtk.FileChooserDialog("Select the path, the name of this, will be the project name", None,
			Gtk.FileChooserAction.SELECT_FOLDER,
			(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
			Gtk.STOCK_OPEN, Gtk.FileChooserAction.OPEN))
		dialog.set_create_folders(True)
		response = dialog.run()
		if response == Gtk.FileChooserAction.OPEN:
			current_path = self.__current_note.return_path()
			new_path = dialog.get_filename()
			print(new_path)
			project_name = new_path.split("/")[-1]
			new_path = new_path + "/" + project_name
			if not os.path.exists(new_path):
				os.makedirs(new_path)
			for fil in self.__current_note.return_files_project():
				file_name = fil.split("/")[-1]
				copy2(fil, new_path+"/"+str(file_name))
			os.rename(str(new_path)+"/"+str(principal_name)+".ino", str(new_path)+"/"+str(project_name)+".ino")
			App().MainWindow.close_project()
			App().MainWindow.open_project(new_path, project_name, True)			
			
		dialog.destroy()
	
	def ig_f(self, dir, files):
		return [f for f in files if os.path.isfile(os.path.join(dir, f))]
	
	def __on_row_activated(self, *args):
		selected = self.__recent_listbox.get_selected_row()
		filename = self.recents_files[selected.get_index()]["file"]	
		base = os.path.basename(filename)
		file_name, file_extension = os.path.splitext(base)
		path = os.path.dirname(filename)
		App().MainWindow.open_project(path, file_name, True)
	
	def return_principal_file(self):
		return self.__current_note.return_principal_file()
	
	def refresh_font(self):
		for note in self.__current_notebooks:
			note.refresh_font()
		
	def reorder_tabs(self, pos):
		if self.__current_note != None and not pos == self.__current_note.get_tab_pos():
			self.__current_note.set_tab_pos(pos)

	def get_current_page(self):
		if self.__current_note != None:
			return self.__current_note.get_page()
		return None
	
	def reload_files(self):
		if self.__current_note != None:
			return self.__current_note.reload_files()
	
	def delete_current_file(self):
		page = self.get_current_page()
		if page != None:
			self.__current_note.remove_tab(page)
			page.delete()
	
	
	
