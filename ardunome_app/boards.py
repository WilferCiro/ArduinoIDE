'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''


import os
import sys
#import config
#import prefs
#import misc
import glob

import gettext
_ = gettext.gettext

try:
	from functions import App
except:
	from ardunome_app.functions import App

from operator import itemgetter

class boardsHandler(object):
	'''
		Init
	'''
	def __init__(self):
		# Paths:
		self.arduino_packages_path = App().Preferences.get_value(App().Preferences.packages_path)
		
		self.boards_general = []
		
		self.readAllBoards()
	
	def returnBoards(self):
		return sorted(self.boards_general, key=itemgetter('filename'), reverse=False)
		#return self.boards_general
	
	def readAllBoards(self):
		self.boards_general = []
		directories = [self.arduino_packages_path]
		all_boards = []
		for directory in directories:
			"""for dir in os.listdir(directory):
				if os.path.isdir(os.path.join(directory, dir)):
					directory2 = directory+"/"+dir+"/hardware"
					for dir2 in os.listdir(directory2):
						if os.path.isdir(os.path.join(directory2, dir2)):
							dir_new = os.path.join(os.path.join(directory2, dir2))
							self.readCustomBoards(dir_new)"""
			boards = glob.glob(directory+"/**/boards.txt", recursive=True)
			all_boards.extend(boards)
		
		for board in all_boards:
			self.readCustomBoards(board)
		
	
	def readCustomBoards(self, directory):
		"""version_folder = None
		for dir in os.listdir(directory):
			if os.path.isdir(os.path.join(directory, dir)):
				version_folder = dir
		"""
		filename = directory #os.path.join(directory, version_folder)+"/boards.txt"
		try:
			file = open(filename, "r")
			current_board = ""
			current_board_name = ""
			for line in file.readlines():
				if line != "" and line[0] != "#":
					text = str(line).replace('\n',"")
					divide_equal = text.split("=")
					if len(divide_equal)>0:
						divide = divide_equal[0].split(".")
						if len(divide) == 2 and divide[0] != current_board:
							if divide[1] == "name":
								if not self.hasSubBoards(divide[0], filename):
									dictionary = dict()
									dictionary["name"] = divide_equal[1]
									dictionary["id"] = divide[0]
									dictionary["filename"] = filename
									self.boards_general.append(dictionary)
								current_board = divide[0]
								current_board_name = divide_equal[1]
						elif len(divide) == 4:
							if divide[1] == "menu" and divide[2] == "cpu":
								dictionary = dict()
								dictionary["name"] = current_board_name+"("+divide_equal[1]+")"
								dictionary["id"] = divide[0]+":cpu="+divide[3]
								dictionary["filename"] = filename
								self.boards_general.append(dictionary)
								current_board = divide[0]
			file.close()
		except:
			print("Error reading boards.txt")

	def hasSubBoards(self, board_id, filename):
		file = open(filename, "r")
		read = str(file.read()).replace("\n","")
		if read.find(board_id+'.menu.cpu') > 0:
			file.close()
			return True
		else:
			file.close()
			return False
		
	def setPreferencesBoard(self, board_id, filename):
		split_board = board_id.split(":")
		cpu = None
		if len(split_board) == 2:
			split_2 = split_board[1].split("=")
			cpu = split_2[1]
			board_id = split_board[0]
	
		file = open(filename, "r")
		read = str(file.read())#.replace("\n","")
		founds = 0
		start = 0
		end = 0
		dict_upload = dict()
		dict_bootloader = dict()
		dict_build = dict()
		while end != -1 and start != -1:
			start = read.find("\n"+board_id+'.', start+4)
			end = read.find("\n"+board_id+'.', start+4)
			if end != -1 and start != -1:
				text = read[start:end].replace("\n", "")
				div_equal = text.split("=")
				if len(div_equal) == 2:
					div_points = div_equal[0].split(".")
					if len(div_points) == 3:
						if div_points[1] == "bootloader":
							dict_bootloader[div_points[2]] = div_equal[1]
							
						elif div_points[1] == "build":
							dict_build[div_points[2]] = div_equal[1]
							
						elif div_points[1] == "upload":
							dict_upload[div_points[2]] = div_equal[1]
					
					elif len(div_points) == 6:
						if cpu == div_points[3]:
							if div_points[4] == "bootloader":
								dict_bootloader[div_points[5]] = div_equal[1]
								
							elif div_points[1] == "build":
								dict_build[div_points[5]] = div_equal[1]
								
							elif div_points[1] == "upload":
								dict_upload[div_points[5]] = div_equal[1]
		file.close()
		return dict_bootloader, dict_build, dict_upload
	
	
	def get_path_mini(self, path):
		divide = path.split("/")
		path_ret = divide[len(divide) - 5]+":"+divide[len(divide) - 3]
		return path_ret
				
				
