'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''


from gi.repository import Pango, GtkSource, Gtk, GObject

try:
	from functions import return_commands_file
except:
	from ardunome_app.functions import return_commands_file

list = [["Hello", "Hello", None, "None"]]
try:
	with open(return_commands_file(), "rt") as f:
		for line in f:
			sp = line.split('::')
			list.append([sp[0], sp[1], None, sp[3]])
except:
	print("Error to open commands")
class completionProvider(GObject.GObject, GtkSource.CompletionProvider):
	"""
		Init completionProvider
	"""
	proposals = []
	
	def do_get_name(self):
		return 'Autocomplete'

	def do_match(self, context):
		new_text = None
		has, iter = context.get_iter()
		line = iter.get_line()
		if has == True:
			buff = iter.get_buffer()			
			text = buff.get_text(
				buff.get_iter_at_line(line),
				buff.get_iter_at_line(line+1),
				True)
			number = iter.get_line_index()
			spli = text[0:number].replace("\t"," ").split(' ')
			new_text = spli[-1]
		
		if new_text == None or new_text == "":
			return False
		
		self.proposals = []		
		new_list = []
		
		for ls in list:
			if new_text in ls[0]:
				new_list.append(ls)
		
		if len(new_list) == 0:
			return False
					
		for new in new_list:
			item = GtkSource.CompletionItem(label=new[0], text=new[1], icon=new[2], info=new[3])
			self.proposals.append(item)		
		return True

	def do_populate(self, context):
		context.add_proposals(self, self.proposals, True)
		return
		
class text_editor(GtkSource.View):
	"""
        Init text editor
    """
	def __init__(self):
		"""
            Init source view
        """
		GtkSource.View.__init__(self)
		
		# Text Buffer
		self.__text_buffer = GtkSource.Buffer()
		self.__text_buffer.set_highlight_syntax(True)
		self.__text_buffer.set_highlight_matching_brackets(True)
		
		# Style Text Buffer
		self.__sm = GtkSource.StyleSchemeManager()
		self.__text_buffer.set_style_scheme(self.__sm.get_scheme("oblivion"))

		# Initial Attributes
		self.set_buffer(self.__text_buffer)
		self.set_show_line_numbers(True)
		self.set_hexpand(True)
		self.set_vexpand(True)
		self.set_highlight_current_line(True)
		self.set_auto_indent(True)
		self.set_smart_home_end(True)
		self.set_insert_spaces_instead_of_tabs(False)
		self.set_tab_width(4)
		self.set_can_focus(True)
		self.set_wrap_mode(Gtk.WrapMode.CHAR)
		self.set_cursor_visible(True)
		
		# Autocomplete
		self.view_completion = self.get_completion()
		custom_completion_provider = completionProvider()
		self.view_completion.add_provider(custom_completion_provider)
		self.custom_completion_provider = custom_completion_provider

		# Languaje manager as Arduino
		self.set_lang("arduino")
		
	def set_text(self, text):
		self.__text_buffer.set_text(text)
	
	def get_text(self):
		text = self.__text_buffer.get_text(
			self.__text_buffer.get_start_iter(),
			self.__text_buffer.get_end_iter(),
			True)
		return text	
	
	def get_buffer(self):
		return self.__text_buffer
	
	def set_color_scheme(self, name):
		self.__text_buffer.set_style_scheme(self.__sm.get_scheme(name))
		
	def set_lang(self, lang):
		lang_manager = GtkSource.LanguageManager()
		if not lang_manager.get_language(lang):
			lang = "cpp"
		self.__text_buffer.set_language(lang_manager.get_language(lang))
	
	def printe(self):
		print("Printing")
