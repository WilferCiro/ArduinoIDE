'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

from gi.repository import Gtk

# Custom Widgets
try:
	from text_editor import text_editor
	from directories_files import directories
	from functions import App
except:
	from ardunome_app.text_editor import text_editor
	from ardunome_app.directories_files import directories
	from ardunome_app.functions import App
	
class tabs(Gtk.ScrolledWindow):
	"""
        Init text editor
    """
	def __init__(self, path, file_name, label_notebook):
		"""
		Init source view
		"""
		Gtk.ScrolledWindow.__init__(self)  
		
		self.__directories_class = directories()    
		# Notebook
		self.label_notebook = label_notebook 
		
		# Control vars
		self.__path = path
		self.__file_name = file_name
		self.__edited = False
		self.__first = True

		# text view
		self.__text_view = text_editor()
		self.buffer = self.__text_view.get_buffer()
		self.buffer.connect("changed", self.update_page_title)
	
		text = self.__directories_class.read_file(path+"/"+file_name)
		self.__text_view.set_text(text)		
		
		# Body
		self.set_border_width(1)
		self.add(self.__text_view)	
		self.show_all()
		
		self.put_scheme()
	
	def __del__(self):
		del self.__text_view
	
	def return_label_tab(self):
		return self.label_notebook
	
	def delete(self):
		self.__directories_class.delete_file(self.__path+"/"+self.__file_name)
		del self
	
	def get_buffer(self):
		return self.buffer	

	def return_path(self):
		return self.__path
	
	def return_file_name(self):
		return self.__file_name
	
	def return_textview_text(self):
		return self.__text_view.get_text()
	
	def return_path_name(self):
		return self.__path+"/"+self.__file_name
	
	def save_page(self):
		if self.__edited == True:
			content = self.return_textview_text()
			self.__directories_class.save_file(self.__path, self.__file_name, content)
			self.label_notebook.set_label(self.__file_name)
	
	def update_page_title(self, *args):
		if not self.__first:
			self.label_notebook.set_label(self.__file_name+" * ")
			self.__edited = True
		else:
			self.__first = False
	
	def is_principal(self):
		name = self.__path.split("/")[-1]
		if name+".ino" == self.__file_name or name+".pde" == self.__file_name:
			return True
		else:
			return False
			
	def rename(self, new_name):
		if self.__directories_class.rename_file(self.__path, self.__file_name, new_name):
			self.__file_name = new_name
			self.update_page_title()
		else:
			print("Error when rename")
		
	def put_scheme(self):
		self.__text_view.set_color_scheme(App().Preferences.get_value(App().Preferences.editor_theme))
		
