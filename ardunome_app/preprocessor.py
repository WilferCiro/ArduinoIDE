'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''
import tempfile
import glob
import os
try:
	from functions import App, APP_NAME
except:
	from ardunome_app.functions import App, APP_NAME

IDE_VERSION = 10805
IS_ERROR = True
NO_ERROR = False

def get_compiler_paths(uploader_prog):
	# TODO: add all compilers
	tools_path = App().Preferences.get_value(App().Preferences.path_builder)
	pack_tools = ""	
	type_out = None
	compilers_paths = None
	
	if uploader_prog == "avrdude":
		search_gcc = glob.glob(tools_path+'/hardware_packages/tools/**/bin/avr-gcc', recursive=True)
		search_avrdude = glob.glob(tools_path+'/hardware_packages/tools/**/avrdude', recursive=True)
		search_OTA = glob.glob(tools_path+'/hardware_packages/tools/**/arduinoOTA', recursive=True)
		if len(search_OTA) + len(search_avrdude) + len(search_gcc) < 3:
			return None
		path_cc = os.path.dirname(os.path.dirname(search_gcc[0]))
		compilers_paths = [
			"runtime.tools.avr-gcc.path="+path_cc,
			"runtime.tools.avrdude.path="+os.path.dirname(search_avrdude[0]),
			"runtime.tools.arduinoOTA.path="+os.path.dirname(search_OTA[0])
		]
		type_out = "hex"
		
	elif uploader_prog == "bossac":
		compilers_paths = [
			"runtime.tools.bossac.path="+pack_tools+"/arduino/tools/bossac/1.6.1-arduino"
			"runtime.tools.arm-none-eabi-gcc.path="+pack_tools+"/arduino/tools/arm-none-eabi-gcc/4.8.3-2014q1"
		]
		type_out = "bin"
	
	return compilers_paths, type_out
	
def preproccess_compile(prev_tmpdir, current_compiled_path):
	### Extract configs
	cancel = False
	# Show user message
	App().consolePanel.set_action("Compiling...")
	App().consolePanel.set_text("Compiling... wait please\n\n", False)
	
	# Paths
	arduino_path = App().Preferences.get_value(App().Preferences.path_builder)
	pack_path = App().Preferences.get_value(App().Preferences.packages_path)
	builder_path = arduino_path+"/arduino-builder"
	hardware_paths = [
		arduino_path+"/hardware_packages",
		pack_path
	]
	tools_paths = [
		arduino_path+"/tools-builder",
		pack_path
	]
	libraries_paths = [
		App().Preferences.get_value(App().Preferences.libraries_paths)
	]
	
	### file to compile
	file_to_compile = None
	rute_builder = None
	try:
		rute_builder = App().leftPanel.get_path_board()
		file_to_compile, path_principal_file = App().Body.return_principal_file()
	except:
		return IS_ERROR
	if file_to_compile == None or rute_builder == None:
		return IS_ERROR
	# tmp paths
	tmpdir = None
	build_path = None
	build_cache_path = None
	keep_output = App().Preferences.get_value(App().Preferences.keep_output)
	if not keep_output:
		if current_compiled_path != file_to_compile:
			tmpdir = tempfile.mkdtemp(APP_NAME)
			prev_tmpdir = tmpdir
		else:
			tmpdir = prev_tmpdir
		build_path = tmpdir
		build_cache_path = tmpdir
	else:
		build_path = path_principal_file+"/Build"
		build_cache_path = path_principal_file+"/Data"	
	# fill paths
	if build_path == "" or build_path == None:
		tmpdir = tempfile.mkdtemp(APP_NAME)
		prev_tmpdir = tmpdir
		build_path = tmpdir
	if not os.path.exists(build_path):
		os.makedirs(build_path)
	if not os.path.exists(build_cache_path):
		os.makedirs(build_cache_path)
	
	if tmpdir == None or build_path == None or build_cache_path == None:
		return IS_ERROR
	
	### Config Board, build options
	build_options, upload_options = App().leftPanel.get_upload_board()
	if upload_options == None or upload_options == None:			
		return IS_ERROR	
	uploader_prog = upload_options['tool']
	if uploader_prog == None or uploader_prog == "":
		return IS_ERROR
	
	### Compilers Paths
	compilers_paths, type_out = get_compiler_paths(uploader_prog)
	if compilers_paths == None or type_out == None:
		return IS_ERROR
	
	### Make the command to compile
	# Start Command
	compile_cmd = []
	get_warnings = "none"
	
	# Verbose
	show_compile_verbose = App().Preferences.get_value(App().Preferences.setting_compile)
	
	# Builder Path - program
	compile_cmd.append(builder_path)
	
	# Opciones de compilación, and logger = human
	# Compile_cmd.append(" -dump-prefs -logger=humantags")
	compile_cmd.append(" -compile -logger=humantags")
	
	# Hardware paths
	for hard in hardware_paths:
		if hard != None:
			compile_cmd.append(" -hardware "+hard)
	for tool in tools_paths:
		if tool != None:
			compile_cmd.append(" -tools "+tool)
	for lib in libraries_paths:
		if lib != None:
			compile_cmd.append(" -libraries "+lib)
	
	# Compile options
	compile_cmd.append(" -fqbn="+rute_builder+" -ide-version="+str(IDE_VERSION))
	compile_cmd.append(" -build-path "+build_path)
	compile_cmd.append(" -warnings="+get_warnings)
	compile_cmd.append(" -build-cache "+build_cache_path)
	compile_cmd.append(" -prefs=build.warn_data_percentage=75")
	
	# Append compiler path
	for comp in compilers_paths:
		compile_cmd.append(" -prefs="+comp)
	# set verbose
	if show_compile_verbose == True:	
		compile_cmd.append(" -verbose")
	
	# Append file_to_compile
	compile_cmd.append(" "+file_to_compile)
	
	# Hex path	
	separate_file = file_to_compile.split("/")
	out_hex = build_path+"/"+separate_file[-1]+"."+type_out
	
	out = dict()
	out["compile_cmd"] = compile_cmd
	out["prev_tmpdir"] = prev_tmpdir
	out["type_out"] = type_out
	out["current_compiled_path"] = file_to_compile
	out["out_hex"] = out_hex
	
	## Return result
	return NO_ERROR, out


def get_uploader_path(uploader_prog):
	arduino_path = App().Preferences.get_value(App().Preferences.path_builder)
	### compare uploaders
	if uploader_prog == "avrdude":
		search_avrdude = glob.glob(arduino_path+'/hardware_packages/tools/**/avrdude', recursive=True)
		search_avrdude_config = glob.glob(arduino_path+'/hardware_packages/tools/**/avrdude.conf', recursive=True)
		if len(search_avrdude) + len(search_avrdude_config) < 2:
			return [IS_ERROR]
		return [search_avrdude[0], search_avrdude_config[0]]

def preproccess_upload(hex_file, port):
	## Check hex file
	if hex_file == "" or hex_file == None or not os.path.isfile(hex_file):
		return IS_ERROR
	## Show to user
	App().consolePanel.set_action("Uploading...")
	## Build options
	build_options, upload_options = App().leftPanel.get_upload_board()
	if upload_options == None or build_options == None:	
		return IS_ERROR
	# Build tool
	uploader_prog = upload_options['tool']
	if uploader_prog == None or uploader_prog == "":
		print("uploader tool")
		return IS_ERROR
	
	# Verbose
	show_upload_verbose = App().Preferences.get_value(App().Preferences.setting_upload)
	
	#try:
	upload_cmd = []
	if uploader_prog == "avrdude":
		# check data
		if build_options['mcu'] == None or build_options['mcu'] == "" or \
			build_options['core'] == None or build_options['core'] == "" or \
			upload_options['speed'] == None or upload_options['speed'] == "":
			print("build options")
			return IS_ERROR
		# Get paths and assign
		paths = get_uploader_path("avrdude")
		if paths[0] == IS_ERROR:
			print("avrdude")
			return IS_ERROR
			
		avrdude_path = paths[0]
		avrdude_conf_path = paths[1]
		# set options
		upload_cmd.append(avrdude_path)
		upload_cmd.append(" -C"+avrdude_conf_path)
		if show_upload_verbose:
			upload_cmd.append(" -v")
		upload_cmd.append(" -p"+build_options['mcu']+" -c"+build_options['core'])
		upload_cmd.append(" -P"+port)
		upload_cmd.append(" -b"+upload_options['speed'])
		upload_cmd.append(" -D -Uflash:w:"+hex_file+":i")
	
	elif uploader_prog == "bossac":
		# get uploader
		[paths] = get_uploader_path("bossac")
		if paths[0] == IS_ERROR:
			return IS_ERROR
			
		bossac_path = paths[0]			
		upload_cmd.append(bossac_path)
		upload_cmd.append(" -i -d")
		upload_cmd.append(" --port="+port.replace("/dev/",""))
		upload_cmd.append(" -U false -e -w -b")
		if show_upload_verbose:
			upload_cmd.append(" -v") 
		upload_cmd.append(" "+hex_file+" -R")				
	
	return upload_cmd
		
		
	#except:
	#	print("except")
	#	return IS_ERROR


