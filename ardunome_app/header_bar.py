'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''


from gi.repository import Gtk, Gio, GObject, Gdk

# Custom Widgets
try:
	from functions import App, return_ui_file
	from text_editor import text_editor
	from recent_manager import recent_manager
except:
	from ardunome_app.functions import App, return_ui_file
	from ardunome_app.text_editor import text_editor
	from ardunome_app.recent_manager import recent_manager
	
import os

import threading

class header_bar(Gtk.Bin):
	"""
        Init text editor
    """
	def __init__(self):
		"""
            Init source view
        """
		Gtk.Bin.__init__(self)
		
		# Configure file
		path = return_ui_file("header_bar.ui")
		self.builder = Gtk.Builder()
		self.builder.add_from_file(path)
		self.builder.connect_signals(self)
		
		self.add(self.builder.get_object("HeaderBar"))
		
		# Recent Manager
		self.__recent_manager = recent_manager()
		#self.__recent_manager.update_recent_files_view(self.__recent_listbox)
		self.recents_files = []
		
		#**** Header Buttons ****#
		# Save Button
		self.__save_button = self.builder.get_object("SaveDirect")
		self.__save_button.connect("clicked", self.__on_save_project)
		# Upload Button
		self.__upload_button = self.builder.get_object("UploadDirect")
		self.__upload_button.connect("clicked", self.on_upload)
		# Compile Button
		self.__compile_button = self.builder.get_object("CompileDirect")
		self.__compile_button.connect("clicked", self.on_compile)
		# New Project
		self.__create_project_button = self.builder.get_object("CreateProjectButton")
		self.__create_project_button.connect("clicked", self.__on_create_project)
		# New Files
		self.__create_file_button = self.builder.get_object("CreateFileButton")
		self.__create_file_button.connect("clicked", self.__on_create_files)		
		# Open Project
		self.__open_project_button = self.builder.get_object("OpenDocumentsButton")
		self.__open_project_button.connect("selection-changed", self.__on_open_project)
		# Open serial window
		self.__open_serial_button = self.builder.get_object("OpenSerialPort")
		self.__open_serial_button.connect("clicked", self.__on_open_serial)
		# Show, Hide left panel
		self.__left_panel_button = self.builder.get_object("LeftPanelOcultShow")
		self.__left_panel_button.connect("clicked", self.__on_panel)
		# Combo Box Text
		self.__combo_projects = self.builder.get_object("ComoBoxProjects")
		self.__combo_projects.connect("changed", self.__on_project_changed)
		# Search Entry
		self.__search_entry = self.builder.get_object("SearchEntry")
		self.__search_entry.connect("search-changed", self.__on_update_recents)
		# Recents entry
		self.__recent_listbox = self.builder.get_object("ListBoxRecents")
		self.__recent_listbox.connect("row-activated", self.__on_row_activated)
		# Menu Edit
		self.__button_copy = self.builder.get_object("CopyButton")
		self.__button_copy.connect("clicked", self.__on_copy)
		self.__button_paste = self.builder.get_object("PasteButton")
		self.__button_paste.connect("clicked", self.__on_paste)
		self.__button_search = self.builder.get_object("SearchButton")
		self.__button_search.connect("clicked", self.on_search)
		self.__button_preferences = self.builder.get_object("PreferencesButton")
		self.__button_preferences.connect("clicked", self.__on_preferences)
		self.__button_reload_files = self.builder.get_object("ReloadFilesButton")
		self.__button_reload_files.connect("clicked", self.__on_reload_files)
		self.__button_shortcuts = self.builder.get_object("ShortcutsButton")
		self.__button_shortcuts.connect("clicked", self.on_open_shortcuts)
		self.__button_delete = self.builder.get_object("DeleteButton")
		self.__button_delete.connect("clicked", self.on_delete_file)
		self.__button_rename = self.builder.get_object("RenameButton")
		self.__button_rename.connect("clicked", self.on_rename_file)
		# Console log
		self.hilo1 = None
		# Search Window
		self.__search_window = self.builder.get_object("SearchWindow")
		self.__pop_text_search = self.builder.get_object("PopTextToSearch")
		self.__pop_text_replace = self.builder.get_object("PopTextToReplace")
		self.__pop_search_button = self.builder.get_object("PopSearchButton")
		self.__pop_search_button.connect("clicked", self.__on_search_button)
		self.__pop_replace_button = self.builder.get_object("PopReplaceButton")
		self.__pop_replace_button.connect("clicked", self.__on_replace_button)
		self.__pop_replace_all_button = self.builder.get_object("PopReplaceAllButton")
		self.__pop_replace_all_button.connect("clicked", self.__on_replace_all_button)
		# Rename popover
		self.__popover_rename = self.builder.get_object("PopoverRename")
		self.__popover_rename_button = self.builder.get_object("ButtonRenamePop")
		self.__popover_rename_button.connect("clicked", self.on_rename_popover)
		
		self.prev_state_left_panel = True
		
		self.found = None
		
		self.update_recents()
		self.show_all()
		
		
	
	#################
	#	PRIVATE 	#
	#################
	
	def on_rename_popover(self, *args):
		self.__popover_rename.hide()
		input = self.builder.get_object("NameRename")
		new_name = input.get_text()
		page = App().Body.get_current_page()
		page.rename(new_name)		
	
	def on_rename_file(self, *args):
		page = App().Body.get_current_page()
		if page.is_principal():
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.OK, "Invalid operation?")
			dialog.format_secondary_text("This is the principal file of the project and can't be renamed.")
			dialog.run()				
			dialog.destroy()
			return 
		if page != None:
			label = self.builder.get_object("LabelRename")
			label.set_text("Rename: "+page.return_file_name())
			input = self.builder.get_object("NameRename")
			input.set_text(page.return_file_name())
			self.__popover_rename.set_relative_to(page.return_label_tab())
			self.__popover_rename.set_position(Gtk.PositionType.BOTTOM)
			self.__popover_rename.show()
	
	def on_delete_file(self, *args):
		page = App().Body.get_current_page()
		if page.is_principal():
			dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.OK, "Invalid operation?")
			dialog.format_secondary_text("This is the principal file of the project and can't be removed.")
			dialog.run()				
			dialog.destroy()
			return
		dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.OK_CANCEL, "Are you sure?")
		dialog.format_secondary_text("This will delete the file shown in the actual tab.")
		response = dialog.run()
		if response == Gtk.ResponseType.OK:
			App().Body.delete_current_file()
		dialog.destroy()
	
	def on_open_shortcuts(self, *args):
		path = return_ui_file("Shortcuts.ui")
		Nbuilder = Gtk.Builder()
		Nbuilder.add_from_file(path)
		window = Nbuilder.get_object("shortcuts")
		window.show_all()
	
	def search_text(self, search_str, buffer):
		if buffer != None:
			start_iter = self.found[1] if self.found else buffer.get_start_iter()
			match = Gtk.TextSearchFlags.CASE_INSENSITIVE
			if self.builder.get_object("PopMatchUpperCase").get_active():
				match = 0
			self.found = start_iter.forward_search(search_str, match, None)
			if self.found:	
				match_start, match_end = self.found
				return match_start, match_end
		return None, None
	
	def __on_search_button(self, *args):
		search_str = self.__pop_text_search.get_text()
		page = App().Body.get_current_page()
		buffer = page.get_buffer()
		match_start, match_end = self.search_text(search_str, buffer)
		if match_start != None and match_end != None:
			buffer.select_range(match_start, match_end)
	
	
	def replace_text(self, search_str, replace_str, individual):
		page = App().Body.get_current_page()
		buffer = page.get_buffer()
		
		if individual == False:
			match_start, match_end = self.search_text(search_str, buffer)
		else:
			if self.found == None or individual == False:
				match_start, match_end = self.search_text(search_str, buffer)
			else:
				match_start, match_end = self.found
		if match_start != None and match_end != None:	
			buffer.place_cursor(match_start)
			buffer.select_range(match_start, match_end)
			buffer.delete_selection(False, True)
			buffer.insert_at_cursor(replace_str)
			self.found = None
		return match_start
		
	def __on_replace_button(self, *args):
		search_str = self.__pop_text_search.get_text()	
		replace_str = self.__pop_text_replace.get_text()
		self.replace_text(search_str, replace_str, True	)
	
	def __on_replace_all_button(self, *args):
		search_str = self.__pop_text_search.get_text()	
		replace_str = self.__pop_text_replace.get_text()
		replaced = ""
		while replaced != None:
			replaced = self.replace_text(search_str, replace_str, False)
		self.found = None
	
	def __on_copy(self, *args):
		page = App().Body.get_current_page()
		buffer = page.get_buffer()
		if buffer != None:
			clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
			buffer.copy_clipboard(clipboard)
	
	def __on_paste(self, *args):
		page = App().Body.get_current_page()
		buffer = page.get_buffer()
		if buffer != None:
			clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
			buffer.paste_clipboard(clipboard, None, True)
	
	def on_search(self, *args):
		self.__search_window.set_relative_to(self)
		self.__search_window.show()
	
	def short_new_file(self, *args):
		win = self.builder.get_object("NewFileDialog")
		win.show()
		
	
	def __on_preferences(self, *args):
		App().Preferences.start()	
	
	
	def __on_reload_files(self, *args):
		App().Body.reload_files()
	
	
	def change_icons(self, cancel):
		img_upload = self.builder.get_object("UploadImage")
		img_compile = self.builder.get_object("CompileImage")
		if cancel:
			img_upload.set_from_icon_name("window-close-symbolic", 1)
			img_compile.set_from_icon_name("window-close-symbolic", 1)
			self.__upload_button.set_tooltip_text("Cancel operation (Ctrl + U)")
			self.__compile_button.set_tooltip_text("Cancel operation (Ctrl + R)")
		else:
			img_upload.set_from_icon_name("go-last-symbolic", 1)
			img_compile.set_from_icon_name("emblem-ok-symbolic", 1)
			self.__upload_button.set_tooltip_text("Compile and Upload (Ctrl + U)")
			self.__compile_button.set_tooltip_text("Compile (Ctrl + R)")
	
	def disable_icons(self, sensitive):
		self.__upload_button.set_sensitive(sensitive)
		self.__compile_button.set_sensitive(sensitive)
		self.__create_file_button.set_sensitive(sensitive)
		self.__save_button.set_sensitive(sensitive)
		App().leftPanel._button_save_copy.set_sensitive(sensitive)
		App().leftPanel._button_close_project.set_sensitive(sensitive)
		self.__button_copy.set_sensitive(sensitive)
		self.__button_paste.set_sensitive(sensitive)
		self.__button_search.set_sensitive(sensitive)
		self.__button_delete.set_sensitive(sensitive)
		self.__button_rename.set_sensitive(sensitive)
		self.__button_reload_files.set_sensitive(sensitive)		
	
	def __on_project_changed(self, *args):
		tree_iter = self.__combo_projects.get_active_iter()
		path = None
		name = None
		if tree_iter is not None:
			model = self.__combo_projects.get_model()
			name, project_id, path = model[tree_iter][:3]
			App().MainWindow.set_open_project(project_id)
			App().consolePanel.set_path(path)
	
	def __on_create_project(self, *args):
		entry = self.builder.get_object("NewProjectName")
		folder = self.builder.get_object("NewProjectPath")
		project_name = entry.get_text()
		path = folder.get_filename()+"/"+project_name
		App().MainWindow.create_project(path, project_name)
		self.__recent_manager.add_recent_file(path+"/"+project_name+".ino")
		self.update_recents()
	
	def on_compile(self, *args):
		self.change_icons(True)
		self.__on_save_project(None)
		App().consolePanel.set_text("", False)
		if self.hilo1 == None or not self.hilo1.is_alive():
			self.hilo1 = threading.Thread(target=App().compileUpload.compile)
			self.hilo1.start()
		else:
			App().compileUpload.cancelCmd()
			App().consolePanel.set_text("Canceled", False)			
	
	def on_upload(self, *args):
		self.change_icons(True)
		App().consolePanel.set_text("", False)
		if self.hilo1 == None or not self.hilo1.is_alive():
			self.hilo1 = threading.Thread(target=App().compileUpload.uploadCode)
			self.hilo1.start()
		else:
			App().compileUpload.cancelCmd()
			App().consolePanel.set_text("Canceled", False)
	
	def __on_save_project(self, *args):
		App().Body.save_project()
	
	def add_project(self, path, name, _id):
		model = self.__combo_projects.get_model()
		model.append([name, _id, path])
		self.__combo_projects.set_active(len(model)-1)
		App().consolePanel.set_path(path)
	
	def __on_open_project(self, *args):
		# TODO: Look if is .ino or .pde, if True: Look project and open, if False: Look folder for .ino or .pde, else, error 
		
		filename = self.__open_project_button.get_filename()
		base = os.path.basename(filename)
		file_name, file_extension = os.path.splitext(base)
		path = os.path.dirname(filename)
		dire = path.split(os.sep)
		parent_dir = dire[len(dire)-1]
		
		if file_extension == '.ino' or file_extension == '.pde':
			if parent_dir == file_name:
				App().MainWindow.open_project(path, file_name, True)
				self.__recent_manager.add_recent_file(filename)
				self.update_recents()
			else:
				print("Create Dir and open project")	
		else:
			print("Revisar los demas archivos a ver :v")
	
	def __on_create_files(self, *args):
		entry = self.builder.get_object("NewName")
		check_class = self.builder.get_object("IsClassNew")
		file_name = entry.get_text()
		is_class = check_class.get_active()
		App().MainWindow.add_file(file_name, is_class)
	
	def close_current_project(self):
		model = self.__combo_projects.get_model()		
		tree_iter = self.__combo_projects.get_active_iter()
		model.remove(tree_iter)
		
		self.__combo_projects.set_active(len(model)-1)
		tree_iter = self.__combo_projects.get_active_iter()
		path = None
		name = None
		project_id = None
		if tree_iter is not None:			
			model = self.__combo_projects.get_model()
			name, project_id, path = model[tree_iter][:3]		
			App().MainWindow.set_open_project(project_id)
			App().consolePanel.set_path(path)
		else:
			App().consolePanel.set_path("Home Page")		
		return project_id
	
	def __on_open_serial(self, *args):
		App().MainWindow.open_serial()
	
	def __on_panel(self, *args):
		if self.__left_panel_button.get_active():
			App().MainWindow.left_panel_show_hide(225)
		else:
			App().MainWindow.left_panel_show_hide(0)
	
	def update_recents(self):
		try:
			while self.__recent_listbox.get_children()[0]:
				self.__recent_listbox.remove(self.__recent_listbox.get_children()[0])
		except:
			None
		search_val = self.__search_entry.get_text()
		if search_val is not None and search_val != "":
			self.recents_files = self.__recent_manager.update_recent_files_view(self.__recent_listbox, search_val)
		else:
			self.recents_files = self.__recent_manager.update_recent_files_view(self.__recent_listbox)
	def __on_update_recents(self, *args):
		self.update_recents()
		
	def __on_row_activated(self, *args):
		selected = self.__recent_listbox.get_selected_row()
		filename = self.recents_files[selected.get_index()]["file"]	
		base = os.path.basename(filename)
		file_name, file_extension = os.path.splitext(base)
		path = os.path.dirname(filename)		
		App().MainWindow.open_project(path, file_name, True)
	
	def disable_left_panel(self, active):
		if active == True:
			active = self.prev_state_left_panel
		else:
			self.prev_state_left_panel = True
		print(active)
		self.__left_panel_button.set_active(active)
		
	
	
