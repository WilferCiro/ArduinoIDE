'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''


import subprocess
import tempfile
import time
import os
import glob
try:
	from functions import App, APP_NAME
	from preprocessor import preproccess_compile, preproccess_upload, IS_ERROR
except:
	from ardunome_app.functions import App, APP_NAME
	from ardunome_app.preprocessor import preproccess_compile, preproccess_upload, IS_ERROR

class compileUpload():

	def __init__(self):
		print("Compiler start")
		self.project_dir = ""
		self.arduino_path = App().Preferences.get_value(App().Preferences.path_builder)
		self.builder_path = self.arduino_path+"/arduino-builder"
		self.pack_path = App().Preferences.get_value(App().Preferences.packages_path)

		self.hardware_paths = [
			self.arduino_path+"/hardware_packages",
			self.pack_path
		]

		self.tools_paths = [
			self.arduino_path+"/tools-builder",
			self.pack_path
		]

		self.libraries_paths = [
			App().Preferences.get_value(App().Preferences.libraries_paths)
		]

		self.IDE_VERSION = 10805
		self.process = None
		self.canceled = False
		self.current_compiled_path = ""
		self.prev_tmpdir = ""
	
	### COLORS:
	# relative things #215d9c
	# Errors #d01531
	# Accepts: #7ba722
	# Warnings: #f9c02f
	
	def showError(self, text):
		# TODO: verificar colores y errores
		has_error = False
		
		# Warnings
		text = text.replace("In file included from", "<span color='#f9c02f'>In file included from</span>")
		
		# Errors
		text = text.replace("error", "<span color='#d01531'>error</span>")
		text = text.replace("^", "<span color='#d01531'>^</span>")
		text = text.replace("exit status 1", "<span color='#d01531'>exit status 1</span>")
		if "error" in text or "exit status 1" in text:
			has_error = True
		# relative things
		text = text.replace("|--", "<span color='#7ba722'>"+(" -"*20)+"</span>")		
		text = text.replace("bytes", "<span color='#215d9c'>bytes</span>")
		text = text.replace("Compiler command:", "<span color='#215d9c'>Compiler command:</span>")
		
		# Uploading
		text = text.replace("avrdude done.  Thank you.", "<span color='#7ba722'>avrdude done.  Thank you.</span>")
		text = text.replace("avrdude: ", "<span color='#215d9c'>avrdude: </span>")
		text = text.replace("Writing", "<span color='#7ba722'>Writing</span>")
		text = text.replace("Reading", "<span color='#7ba722'>Reading</span>")
		if not self.canceled:
			App().consolePanel.set_text(text, False)
		return has_error
		
	def uploadCode(self):
		self.canceled = False
		### Serial port get
		port = str(App().leftPanel.get_active_port())
		# Verify free		
		free_port, error_txt = App().VisualPort.verify_port(port)
		if free_port == False:
			self.finisedAction()
			App().consolePanel.set_text("<span color='red'>"+error_txt+"</span>", False)
			return
		
		### compile command		
		try:
			out_text, hex_file = self.compile(True)
			if self.process is not None or out_text == None:
				self.cancelCmd()
				return
		except:
			self.cancelCmd()
			return
		
		## Upload Command
		upload_cmd = preproccess_upload(hex_file, port)
		if upload_cmd == "" or upload_cmd == IS_ERROR or upload_cmd == None:
			self.cancelCmd()
			return
		
		# execute
		cmdline = "".join(upload_cmd)
		text = out_text+ "\n\n|--\n" +"\nUpload command:\n"+cmdline + "\n\n|--\n"
		text = text + self.excecuteCmd(cmdline)		
		self.showError(text)
		self.finisedAction()		

	def compile(self, upload_previus = False):
		self.canceled = False
		# check if is an active compilation	
		if self.process is not None:
			self.cancelCmd()
			return		
		try:
			# Get compile cmd
			error, out = preproccess_compile(self.prev_tmpdir, self.current_compiled_path)
			# check errors
			if error == IS_ERROR:
				self.cancelCmd()
				return
			# assign variables
			compile_cmd = out["compile_cmd"]
			self.prev_tmpdir = out["prev_tmpdir"]
			type_out = out["type_out"]
			self.current_compiled_path = out["current_compiled_path"]
			out_hex = out["out_hex"]
		except:
			self.cancelCmd()
			return
		# join array
		cmdline = "".join(compile_cmd)
		# append output
		out = "<b>Compiler command:</b> \n"
		out = out + cmdline + "\n\n|--\n";
		# Execute command in shell
		out = out + self.excecuteCmd(cmdline)
		# Add hex or bin legend
		out = out+"<span color='green'><b>"+type_out+" file is alocated in: </b></span>"+out_hex+"\n"
		# Show to user the output		
		has_error = self.showError(out)			
		if has_error:
			self.finisedAction()
			return None, None
		# check upload
		if not upload_previus:
			self.finisedAction()
		# Return out and final hex
		return out, out_hex
	
	def finisedAction(self):
		App().consolePanel.set_action(None)	
		App().headerBar.change_icons(False)		
	
	def cancelCmd(self, text = ""):
		self.canceled = True
		if self.process is not None:
			self.process.terminate()
		self.process = None
		file = open('test.log', 'w+')
		file.close()
		self.finisedAction()
		App().consolePanel.set_text("<span color='red'>Canceled operation!\n"+text+"</span>", True)
	
	def excecuteCmd(self, cmdline):
		sout = ""
		soutGeneral = ""
		number = 0		
		try:			
			self.process = subprocess.Popen(cmdline, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)			
			if self.process != None:
				for c in iter(lambda: self.process.stdout.read(1), b''):
					number = number + 1
					try:
						sout = sout + c.decode("utf-8")
						soutGeneral = soutGeneral + c.decode("utf-8")
					except:
						sout = sout
					if number > 50:
						number = 0
						if not self.canceled:
							App().consolePanel.set_text(sout, True)
							time.sleep(0.1)
						sout = ""
					if self.process == None:
						break
						
			if not self.canceled:
				time.sleep(0.5)
				App().consolePanel.set_text(sout+"\n", True)
			time.sleep(0.1)
						
		except subprocess.SubprocessError as e:
			raise RuntimeError('Popen failed: {0.__name__} {1}'.format(type(e), str(e))) from e
		
		self.process = None
		return soutGeneral

