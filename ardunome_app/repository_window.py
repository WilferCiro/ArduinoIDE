'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

from gi.repository import Gtk, Gio, GLib

try:
	from functions import return_ui_file, App
except:
	from ardunome_app.functions import return_ui_file, App
# Json readability
import json
from pprint import pprint
import os
import time
import threading
from contextlib import closing
from urllib.request import urlopen, urlretrieve

#FIXME: all xD

class repository_window(Gtk.Window):
	"""
        Init text editor
    """
	def __init__(self):
		"""
            Init source view
        """
		Gtk.Window.__init__(self)
		
		# Configure file
		path = return_ui_file("shop.ui")
		self.builder = Gtk.Builder()
		self.builder.add_from_file(path)
		self.builder.connect_signals(self)
																																																	
		self.connect("delete_event", self.close_preferences)
		self.set_position(Gtk.WindowPosition.CENTER)
		self.set_default_size(1030, 630)
		self.maximize()
		
		self.__header = self.builder.get_object("RepositoryHeader")
		self.__body = self.builder.get_object("RepositoryBody")
		self.set_titlebar(self.__header)
		self.set_modal(True)
		self.add(self.__body)
		
		self.page_size = 10
		
		# Visual components
		self.como_box_page_boards = self.builder.get_object("BoardsPageSelector")
		self.como_box_page_boards.connect("changed", self.change_list)
		self.boards_list = self.builder.get_object("BoardsBox")
		self.progress_download = self.builder.get_object("ProgressDownload")
		# Toggle
		self.toggle_boards = self.builder.get_object("ToggleBoards")
		self.toggle_boards.connect("toggled", self.change_page_board)
		self.toggle_libraries = self.builder.get_object("ToggleLibraries")
		self.toggle_libraries.connect("toggled", self.change_page_library)
		# Cancel action
		self.cancel_button = self.builder.get_object("CancelButton")
		self.cancel_button.connect("clicked", self.cancel_button_press)
		self.cancel_download = False
		self.downloading = False
		# Update
		self.update_button = self.builder.get_object("UpdateRepository")
		self.update_button.connect("clicked", self.update_repositories)
		self.check_libraries = self.builder.get_object("CheckLibrariesUpdate")
		self.check_boards = self.builder.get_object("CheckBoardsUpdate")
		
		# Json download
		self.json_libraries = ["http://downloads.arduino.cc/libraries/library_index.json"]
		self.json_boards = ["http://downloads.arduino.cc/packages/package_index.json"]
		
		self.__all_boards = []
		self.__all_libraries = []
		
		self.boards_downloaded = 0
		self.libraries_downloaded = 0
		self.finished = False
	
	
	def update_repositories(self, *args):
		self.boards_downloaded = 0
		self.libraries_downloaded = 0
		hilo1 = threading.Thread(target=self.download_json)
		hilo1.daemon = True
		hilo1.start()
		GLib.timeout_add(10, self.look_put_elements)
		self.boards_list.set_sensitive(False)
		self.finished = False
	
	def cancel_button_press(self, *args):
		if self.downloading:
			self.cancel_download = True
	
	def change_page_library(self, widget):
		if widget.get_active():
			self.toggle_boards.set_active(False)			
			self.como_box_page_boards.remove_all()
			for page_num in range(0, int(len(self.__all_libraries)/self.page_size)):
				self.como_box_page_boards.append_text("Page: "+str(page_num))
			self.show_boards(0)
		else:
			self.toggle_boards.set_active(True)
		
	def change_page_board(self, widget):
		if widget.get_active():
			self.toggle_libraries.set_active(False)			
			self.como_box_page_boards.remove_all()
			for page_num in range(0, int(len(self.__all_boards)/self.page_size)):
				self.como_box_page_boards.append_text("Page: "+str(page_num))
			self.show_boards(0)
		else:
			self.toggle_libraries.set_active(True)
		
	
	def change_list(self, *args):
		page = self.como_box_page_boards.get_active_text()
		if page != None:
			page = int(page.replace("Page: ",""))
		else:
			page = 0
		self.show_boards(page)
		
	def show_boards(self, page):
		try:
			while self.boards_list.get_children()[0]:
				self.boards_list.remove(self.boards_list.get_children()[0])
		except:
			None
		
		if self.toggle_boards.get_active():
			for board in self.__all_boards[page*self.page_size:(page*self.page_size)+self.page_size]:
				row = Gtk.ListBoxRow()
				
				hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
				
				Image = Gtk.Image()
				Image.set_from_icon_name("face-monkey", 1)
				Image.set_pixel_size(76)
				Image.set_size_request(100, 100)
				
				label_general = Gtk.Label("", xalign=0)
				label_general.set_markup("<b>"+board["board_name"]+"</b>\n<small>By: "+board["provider_name"]+"</small>\n<span color='#7ba722'>Installed</span>")
				label_general.set_size_request(350, -1)
				label_general.set_line_wrap(True)
				text = ""
				arch = ""
				version_box = Gtk.ComboBoxText()
				for bd in board["boards"]:
					text = text + bd["version"]+", "
					if not bd["architecture"] in arch:
						arch = arch + bd["architecture"]+", "
					version_box.append_text(bd["version"])
				
				version_box.set_active(0)
				
				label_description = Gtk.Label("", xalign=0)
				label_description.set_markup("<b>"+ board["board_name"]+"</b>\n<small>Disponible versions: "+text+"</small>\n<small>Architexture: </small>"+arch)
				label_description.set_line_wrap(True)
				
				vBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
				vBox.set_size_request(50, -1)
				Button = Gtk.Button("Install")
				
				Label_web = Gtk.LinkButton(board["provider_websiteURL"],"More infor")
				vBox.pack_start(Label_web, True, True, 0)
				vBox.pack_start(version_box, False, True, 0)
				vBox.pack_start(Button, False, True, 0)
				
				hbox.pack_start(Image, False, True, 0)
				hbox.pack_start(label_general, False, True, 0)
				hbox.pack_start(label_description, True, True, 25)	
				hbox.pack_start(vBox, False, True, 0)			
				row.add(hbox)
				row.show_all()
				self.boards_list.insert(row, -1)
			
		elif self.toggle_libraries.get_active():
			for lib in self.__all_libraries[page*self.page_size:(page*self.page_size)+self.page_size]:
				row = Gtk.ListBoxRow()
				
				hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=2)
				
				Image = Gtk.Image()
				Image.set_from_icon_name("face-monkey", 1)
				Image.set_pixel_size(76)
				Image.set_size_request(100, 100)
				
				label_general = Gtk.Label("", xalign=0)
				label_general.set_markup("<b>"+lib["lib_name"]+"</b>\n<small>By: </small>\n<span color='#7ba722'>Installed</span>")
				label_general.set_size_request(350, -1)
				label_general.set_line_wrap(True)
				text = ""
				version_box = Gtk.ComboBoxText()
				for lb in lib["versions"]:
					text = text + lb["version"]+", "
					version_box.append_text(lb["version"])
				
				version_box.set_active(-1)
				
				label_description = Gtk.Label("", xalign=0)
				label_description.set_markup("<span color='yellow'><b>"+ lib["lib_name"]+"</b></span>\n<small>Disponible versions: "+text+"</small>\n<small>"+lib["versions"][0]["sentence"].replace("\u0026","and")+"</small>")
				label_description.set_line_wrap(True)
				
				vBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
				vBox.set_size_request(50, -1)
				Button = Gtk.Button("Install")
				
				Label_web = Gtk.LinkButton("","More infor")
				vBox.pack_start(Label_web, True, True, 0)
				vBox.pack_start(version_box, False, True, 0)
				vBox.pack_start(Button, False, True, 0)
				
				hbox.pack_start(Image, False, True, 0)
				hbox.pack_start(label_general, False, True, 0)
				hbox.pack_start(label_description, True, True, 25)	
				hbox.pack_start(vBox, False, True, 0)			
				row.add(hbox)
				row.show_all()
				self.boards_list.insert(row, -1)
	
	def close_preferences(self, *args):
		self.cancel_download = True
		return self.hide_on_delete()
	
	def download_json(self):
		try:
			self.progress_download.set_text("Getting json file information")
			if self.boards_downloaded < len(self.json_boards) and self.check_boards.get_active():
				link = self.json_boards[self.boards_downloaded]
				replace = link.split("/")
				self.boards_downloaded = self.boards_downloaded + 1
				urlretrieve(link, replace[-1], self.reporthook)
		
			elif self.libraries_downloaded < len(self.json_libraries) and self.check_libraries.get_active():
				link = self.json_libraries[self.libraries_downloaded]
				replace = link.split("/")
				self.libraries_downloaded = self.libraries_downloaded + 1
				urlretrieve(link, replace[-1], self.reporthook)
			else:
				self.finished = True
		except:
			None
	
	def look_put_elements(self):
		if self.finished == True:
			self.progress_download.set_text("Downloaded")
			self.boards_list.set_sensitive(True)
			self.put_elements()
		else:
			GLib.timeout_add(1000, self.look_put_elements)
	
	def reporthook(self, count, block_size, total_size):
		self.downloading = True
		progress_size = 0
		global start_time
		if count == 0:
			start_time = time.time()
			return
		try:
			duration = time.time() - start_time
			duration = duration if duration > 0 else 1
			progress_size = count * block_size
			speed = int(int(progress_size) / (1024 * duration))
			#percent = int(count * block_size * 100 / total_size)
			percent = float("{0:.2f}".format(progress_size/total_size))
			if percent > 1:
				percent = 1
			self.progress_download.set_fraction(percent)
			self.progress_download.set_text("Downloading "+str(int(percent*100))+"% - "+str(int(progress_size/(2*1024)))+"Kb of "+str(int(total_size/(1024*2)))+" Kb"+" - "+str(speed)+" Kbs")
		except:
			None
		if progress_size >= total_size:
			self.downloading = False
			self.finish_download(True)
		if self.cancel_download:
			self.number_of_downloaded = 0
			self.cancel_download = False
			self.finish_download(False)
			self.downloading = False
			raise TooSlowException		

	def finish_download(self, excelent):
		if excelent:
			self.download_json()
		else:
			self.progress_download.set_text("Canceled Download")
			self.boards_list.set_sensitive(True)
		self.progress_download.set_fraction(0)
			
	def start(self):
		self.put_elements()
		self.show_all()
	
	def put_elements(self):
		self.__all_boards = []
		json_files = App().Preferences.get_value(App().Preferences.repository_url)
		for file in json_files:
			if not os.path.isfile(file):
				self.update_repositories(None)
				print("No encounter")
			else:
				with open('package_index.json') as data_file:    
					data = json.load(data_file)
				for prov in data["packages"]:
					for board in prov["platforms"]:
						board_info = dict()				
						exist = False
						for check_ver in self.__all_boards:
							if check_ver["board_name"] == board["name"]:
								exist = True
								board_info = check_ver
								pass				
						if not exist:
							board_info["provider_name"] = prov["name"]
							board_info["provider_maintainer"] = prov["maintainer"]
							board_info["provider_websiteURL"] = prov["websiteURL"]					
							board_info["board_name"] = board["name"]
							board_info["boards"] = []
						
						individual_board = dict()
						individual_board["architecture"] = board["architecture"]
						individual_board["version"] = board["version"]
						individual_board["url"] = board["url"]
						individual_board["archiveFileName"] = board["archiveFileName"]
						individual_board["checksum"] = board["checksum"]
						individual_board["size"] = board["size"]
						individual_board["checksum"] = board["checksum"]
						individual_board["boards"] = board["boards"]	# Iter
						individual_board["toolsDependencies"] = board["toolsDependencies"] # Iter
						
						board_info["boards"].append(individual_board)
						
						if not exist:
							self.__all_boards.append(board_info)
				data_file.close()
				
				with open('library_index.json') as data_file:    
					data = json.load(data_file)
				
				for lib in data["libraries"]:
					lib_info = dict()			
					exist = False
					for check_ver in self.__all_libraries:
						if check_ver["lib_name"] == lib["name"]:
							exist = True
							lib_info = check_ver
							pass				
					if not exist:
						lib_info["lib_name"] = lib["name"]
						lib_info["versions"] = []
					
					version_info = dict()
					version_info["version"] = lib["version"]
					version_info["author"] = lib["author"]
					version_info["maintainer"] = lib["maintainer"]
					version_info["sentence"] = lib["sentence"]
					try:
						version_info["paragraph"] = lib["paragraph"]
					except:
						None
					version_info["website"] = lib["website"]
					version_info["category"] = lib["category"]
					version_info["repository"] = lib["repository"]
					version_info["url"] = lib["url"]
					version_info["archiveFileName"] = lib["archiveFileName"]
					version_info["size"] = lib["size"]
					version_info["checksum"] = lib["checksum"]
					lib_info["versions"].append(version_info)							
					if not exist:
						self.__all_libraries.append(lib_info)
				data_file.close()
				#self.como_box_page_boards.set_active(0)
				self.show_boards(0)


class TooSlowException(Exception):
    pass		
		

