'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''


from gi.repository import Gtk, Gio

try:
	from functions import return_mime_type, APP_NAME
except:
	from ardunome_app.functions import return_mime_type, APP_NAME
import os

from operator import itemgetter
import datetime

class recent_manager:
	"""
		Init recent files manager
	"""
	
	def __init__(self):
		self.__manager = Gtk.RecentManager.get_default()
	
	def add_recent_file(self, path):
		#data = { 'project_name': project_name, 'path' : path, 'app_name' : "gnoduino", 'app_exec' : "gnoduino" }
		#uri = "file://" + urllib.pathname2url(f)
		#mime = str(return_mime_type(path))
		#print(mime)
		#self.__manager.add_item(path)
		mime_type = return_mime_type(path)
		recent_data = Gtk.RecentData()
		recent_data.app_name = APP_NAME
		recent_data.app_exec = APP_NAME
		recent_data.mime_type = mime_type
		self.__manager.add_full(path, recent_data)
	
	def return_recent_files(self):
		list_recent = []
		for item in self.__manager.get_items():
			if str(item.get_applications()[0]) == APP_NAME:
				file = item.get_uri()
				date = item.get_modified()
				if return_mime_type(file) == "text/x-arduino":
					dictionary = dict()
					dictionary["file"] = file
					dictionary["date"] = date
					list_recent.append(dictionary)
					#list_recent.append(file)
		return sorted(list_recent, key=itemgetter('date'), reverse=False)
	
	def return_recent_files_filter(self, filter):
		list_recent = []
		for item in self.__manager.get_items():
			if str(item.get_applications()[0]) == APP_NAME:
				file = item.get_uri()
				date = item.get_modified()
				if return_mime_type(file) == "text/x-arduino" and filter in file:
					dictionary = dict()
					dictionary["file"] = file
					dictionary["date"] = date
					list_recent.append(dictionary)
		return sorted(list_recent, key=itemgetter('date'), reverse=False)
	
	def update_recent_files_view(self, listbox, filter = None):
		if filter is None:
			recents = self.return_recent_files()
		else:
			recents = self.return_recent_files_filter(filter)
		for rec in recents:
			row = Gtk.ListBoxRow()
			hbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=2)
			
			base = os.path.basename(rec["file"])
			path = os.path.dirname(rec["file"])
			dire = path.split(os.sep)
			parent_dir = dire[len(dire)-1]
			
			label1 = Gtk.Label(parent_dir, xalign=0)
			label1.set_markup("<b>"+parent_dir+"</b> : <small>"+datetime.datetime.fromtimestamp(int(rec["date"])).strftime('%Y-%m-%d %H:%M:%S')+"</small>")
			label2 = Gtk.Label("", xalign=0)
			label2.set_markup("<small>"+rec["file"]+"</small>")
			separator = Gtk.HSeparator()			
			hbox.pack_start(label1, True, True, 0)
			hbox.pack_start(label2, True, True, 0)	
			hbox.pack_start(separator, True, True, 5)			
			row.add(hbox)
			row.show_all()
			listbox.insert(row, -1)
		return recents

