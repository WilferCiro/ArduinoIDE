'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

from gi.repository import Gtk, Gio

try:
	from functions import return_ui_file
except:
	from ardunome_app.functions import return_ui_file

class console(Gtk.Bin):
	"""
        Init text editor
    """
	def __init__(self):
		"""
            Init source view
        """
		Gtk.Bin.__init__(self)
		
		# Configure file
		path = return_ui_file("console_panel.ui")
		self.builder = Gtk.Builder()
		self.builder.add_from_file(path)
		self.builder.connect_signals(self)
		
		self.add(self.builder.get_object("BoxConsoleDebug"))
		
		self.__text_debug = self.builder.get_object("DebugConsoleLabel")
		self.__scroll_output = self.builder.get_object("ScrollOutput")
		
		self.__preview_board = self.builder.get_object("PreviewBoard")
		self.__preview_port = self.builder.get_object("PreviewPort")
		self.__preview_programmer = self.builder.get_object("PreviewProgrammer")
		
		
		self.__current_action = self.builder.get_object("CurrentAction")
		
		self.__directory_path = self.builder.get_object("ProjectPath")
		#UserActionsLabel
		
	def set_text(self, text, conserv):
		other_text = self.__text_debug.get_label()
		if conserv:
			self.__text_debug.set_markup(other_text+"<span font='Monospace'>"+text+"</span>")
		else:
			self.__text_debug.set_markup("<span font='Monospace'>"+text+"</span>")
		adj = self.__scroll_output.get_vadjustment()
		adj.set_value(adj.get_upper() - adj.get_page_size())
	
	def set_preview_board(self, text):
		self.__preview_board.set_label(text)
		
	def set_preview_port(self, text):
		self.__preview_port.set_label(text)
	
	def set_preview_programmer(self, text):
		self.__preview_programmer.set_label(text)
	
	def set_action(self, text):
		if text == None:
			self.__current_action.set_text("Sleeping... 😴")
		else:
			self.__current_action.set_text(text)
			
	def set_path(self, path):
		self.__directory_path.set_text(str(path))
		
		
