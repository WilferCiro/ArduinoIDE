#!/usr/bin/python
'''
	Copyright (c) 2018 Wilfer Daniel Ciro Maya <wilcirom@gmail.com>
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

import os
import sys
import subprocess
from setuptools import setup
import appdirs
home = str(os.environ['HOME'])

def get_gnoduino_version():
	return 0.1

def get_data_files():
	ui_files = []
	for x in os.listdir("ui/"):
		ui_files.append("ui/"+str(x))
	data_files = [
		('/usr/share/ardunome/ui', ui_files),
		(home+"/.config/ardunome", ["data/commands.txt", "data/style.css"]),
		('/usr/share/applications', ['data/ardunome.desktop']),
		('/usr/share/icons/hicolor/scalable/apps', ['data/ardunome.svg']),
	]
	return data_files


setup(name='ardunome',
	python_requires='>=3',
	version=get_gnoduino_version(),
	description='Gnome Arduino IDE implementation',
	package_dir={'ardunome': 'ardunome_app'},
	packages = ['ardunome_app'],
	scripts = ['ardunome_run'],
	author='Wilfer Ciro',
	author_email='wilcirom@gmail.com',
	url='https://gitlab.com/WilferCiro/ArduinoIDE',
	license='GPLv3',
	platforms='linux',
	data_files = get_data_files(),
	install_requires = ['pygobject', 'serial', 'send2trash'],
)


#retcode = subprocess.Popen("echo $(HOME)", shell=True)

